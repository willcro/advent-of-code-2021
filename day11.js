var rawInput = document.body.children[0].innerText.split("\n");
rawInput.pop();

var rawData = rawInput.map(it => it.split("").map(it => it * 1));

var test = `5483143223
2745854711
5264556173
6141336146
6357385478
4167524645
2176841721
6882881134
4846848554
5283751526`.split("\n").map(it => it.split("").map(it => it * 1));

function part1(input, steps) {
    var flashes = 0;
    var data = input;
    for (var i=0; i<steps; i++) {
        var step = runStep(data);
        data = step.data;
        flashes += step.flashes;
    }
    return flashes;
}

/**
 * returns flashes
 */
function runStep(data) {
    var flashes = 0;
    var width = data[0].length;
    var height = data.length;
    var xmax = width - 1;
    var ymax = height - 1;
    data = incrementAll(data);
    var hadFlashes = false;
    do {
        hadFlashes = false;
        for (var y=0; y<height; y++) {
            for (var x=0; x<width; x++) {
                if (data[y][x] > 9) {
                    flashes++;
                    hadFlashes = true;
                    data[y][x] = -Infinity;
                    // left
                    if (x > 0) {
                        data[y][x-1]++;
                    }
                    // right
                    if (x < xmax) {
                        data [y][x+1]++;
                    }
                    // up
                    if (y > 0) {
                        data[y-1][x]++
                    }
                    // down
                    if (y < ymax) {
                        data[y+1][x]++
                    }

                    // top left
                    if (x > 0 && y > 0) {
                        data[y-1][x-1]++;
                    }
                    // top right
                    if (x < xmax && y > 0) {
                        data[y-1][x+1]++;
                    }
                    // bottom left
                    if (x > 0 && y < ymax) {
                        data[y+1][x-1]++;
                    }
                    // bottom right
                    if (x < xmax && y < ymax) {
                        data[y+1][x+1]++;
                    }

                }
            }
        }
    } while (hadFlashes);

    // set all flashed back to 0;
    data = data.map(row => row.map(it => it == -Infinity ? 0 : it));

    return {flashes: flashes, data: data};
}

function incrementAll(data) {
    return data.map(row => row.map(it => it + 1));
}

function part2(input, maxSteps) {
    var data = input;
    for (var i=0; i<maxSteps; i++) {
        var step = runStep(data);
        data = step.data;
        if (step.flashes == 100) {
            return i+1;
        }
    }
    throw "dunno";
}


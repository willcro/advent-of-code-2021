// part 1

// get input list 
var nums = document.body.children[0].innerText.split("\n")

function getPosition(directions) {
    var depth = 0;
    var distance = 0;
    for (var i=0; i<directions.length; i++) {
        var match = directions[i].match(/^(.*) (.*)$/);
        if (match == null) {
            console.warn("could not parse line " + i);
            break;
        }
        var direction = match[1];
        var amount = 1 * match[2];
        if (direction == "up") {
            depth -= amount
        } else if (direction == "down") {
            depth += amount;
        } else if (direction == "forward") {
            distance += amount;
        } else if (direction == "backward") {
            distance -= amount;
        } else {
            throw "unknown direction " + direction;
        }
    }
    console.log(`Depth: ${depth}, Distance: ${distance}`);
    return depth * distance;
}

// part 2

function getPositionPart2(directions) {
    var depth = 0;
    var distance = 0;
    var aim = 0
    for (var i=0; i<directions.length; i++) {
        var match = directions[i].match(/^(.*) (.*)$/);
        if (match == null) {
            console.warn("could not parse line " + i);
            break;
        }
        var direction = match[1];
        var amount = 1 * match[2];
        if (direction == "up") {
            aim -= amount
        } else if (direction == "down") {
            aim += amount;
        } else if (direction == "forward") {
            distance += amount;
            depth += aim * amount;
        } else {
            throw "unknown direction " + direction;
        }
    }
    console.log(`Depth: ${depth}, Distance: ${distance}`);
    return depth * distance;
}

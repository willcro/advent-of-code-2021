package com.willcro.advent.day22;

import com.willcro.advent.day19.Day19;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Day22 {

  public static void main(String[] args) throws IOException {
    var in = Day22.class.getResourceAsStream("/day22main.txt");
    var inputString = new String(in.readAllBytes());
    var input = parseInput(inputString);
    var cubes = new HashSet<Cuboid>();
    for (var i=0; i<input.size(); i++) {
      var thisCube = input.get(i);
      var subCubes = cubes.stream()
          .map(cube -> cube.overlap(thisCube, thisCube.getIsOn()))
          .filter(Objects::nonNull)
          .collect(Collectors.toSet());
      cubes.add(thisCube);
      cubes.addAll(subCubes);
    }

    // find all lit ones
    var lightsOn = cubes.stream().filter(Cuboid::getIsOn).mapToLong(cube -> getAdjustedSize(cube, cubes)).sum();
    System.out.println("Lights on: " + lightsOn);

//    var pixels = new ArrayList<>();
//    cubes.stream().map(cube -> getAdjustedPixels(cube, cubes)).forEach(pixels::addAll);
//
//    var dupes = pixels.stream()
//        .collect(Collectors.groupingBy(it -> it, Collectors.counting()))
//        .entrySet().stream()
//        .filter(it -> it.getValue() > 1)
//        .collect(Collectors.toMap(Entry::getKey, Entry::getValue));

//    var lit = cubes.stream().filter(Cuboid::getIsOn);

//    var naive = litPixels(input);

    return;
  }

  private static Set<Coordinate> litPixels(List<Cuboid> input) {
    var out = new HashSet<Coordinate>();
    for (Cuboid cuboid : input) {
      for (var x=cuboid.getXMin(); x<=cuboid.getXMax(); x++) {
        for (var y=cuboid.getYMin(); y<=cuboid.getYMax(); y++) {
          for (var z=cuboid.getZMin(); z<=cuboid.getZMax(); z++) {
            if (cuboid.getIsOn()) {
              out.add(new Coordinate(x,y,z));
            } else {
              out.remove(new Coordinate(x,y,z));
            }
          }
        }
      }
    }
    return out;
  }

  private static List<Cuboid> parseInput(String input) {
    var lines = Arrays.asList(input.split("\n"));
    var out = new ArrayList<Cuboid>();
    for (var i=0; i<lines.size(); i++) {
      out.add(parseLine(lines.get(i), i));
    }
    return out;
  }

  private static Cuboid parseLine(String line, Integer lineNum){
    var pattern = Pattern.compile("(on|off) x=(-?\\d+)\\.\\.(-?\\d+),y=(-?\\d+)\\.\\.(-?\\d+),z=(-?\\d+)\\.\\.(-?\\d+)");
    var matcher = pattern.matcher(line);
    if (matcher.matches()) {
      var id = String.valueOf(lineNum + 1);
      var isOn = matcher.group(1).equals("on");
      var xMin = Integer.parseInt(matcher.group(2));
      var xMax = Integer.parseInt(matcher.group(3));
      var yMin = Integer.parseInt(matcher.group(4));
      var yMax = Integer.parseInt(matcher.group(5));
      var zMin = Integer.parseInt(matcher.group(6));
      var zMax = Integer.parseInt(matcher.group(7));

      assert xMin <= xMax;
      assert yMin <= yMax;
      assert zMin <= zMax;

      return new Cuboid(id, Set.of(id), isOn, xMin, xMax, yMin, yMax, zMin, zMax);
    }
    throw new RuntimeException("Line was invalid: " + line);
  }

  private static Map<Cuboid, Set<Cuboid>> subCubes = new HashMap<>();

  private static Set<Cuboid> findAllSubCuboids(Cuboid thisCuboid, Set<Cuboid> allCuboids) {
    if (subCubes.containsKey(thisCuboid)) {
      return subCubes.get(thisCuboid);
    }
    var ret = allCuboids.stream()
        .filter(subCube -> subCube.getParents().containsAll(thisCuboid.getParents()))
        .filter(subCube -> !subCube.equals(thisCuboid))
        .collect(Collectors.toSet());
    subCubes.put(thisCuboid, ret);
    return ret;
  }

  private static Map<Cuboid, Long> adjustedSizes = new HashMap<>();

  private static Long getAdjustedSize(Cuboid thisCuboid, Set<Cuboid> allCuboids) {
    if (adjustedSizes.containsKey(thisCuboid)) {
      return adjustedSizes.get(thisCuboid);
    }

    var subCuboids = findAllSubCuboids(thisCuboid, allCuboids);
    var sizeAdjustment = subCuboids.stream().mapToLong(subCube -> getAdjustedSize(subCube, allCuboids)).sum();

    var ret = thisCuboid.getSize() - sizeAdjustment;
    assert ret >= 0;
    adjustedSizes.put(thisCuboid, ret);

    return ret;
  }

  private static Map<Cuboid, Set<Coordinate>> adjustedPixels = new HashMap<>();

  private static Set<Coordinate> getAdjustedPixels(Cuboid thisCuboid, Set<Cuboid> allCuboids) {
    if (adjustedPixels.containsKey(thisCuboid)) {
      return adjustedPixels.get(thisCuboid);
    }

    var pixels = thisCuboid.getAllPixels();

    var subCuboids = findAllSubCuboids(thisCuboid, allCuboids);
    subCuboids.stream()
        .map(cube -> getAdjustedPixels(cube, allCuboids))
        .forEach(pixels::removeAll);


    adjustedPixels.put(thisCuboid, pixels);

    return pixels;
  }

}

package com.willcro.advent.day22;

import lombok.Data;

@Data
public class Coordinate {

  private final Integer x;
  private final Integer y;
  private final Integer z;

}

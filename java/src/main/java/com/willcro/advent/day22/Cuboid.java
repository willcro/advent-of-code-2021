package com.willcro.advent.day22;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.Data;

@Data
public class Cuboid {

  private final String id;
  private final Set<String> parents;
  private final Boolean isOn;
  private final Integer xMin;
  private final Integer xMax;
  private final Integer yMin;
  private final Integer yMax;
  private final Integer zMin;
  private final Integer zMax;

  private Set<Cuboid> subCuboids = new HashSet<>();

  public Cuboid overlap(Cuboid other, Boolean isOn) {
    var otherVerts = other.getVertices();
    var thisContainsOther = otherVerts.stream().anyMatch(this::contains);
    var otherContainsThis = this.getVertices().stream().anyMatch(other::contains);
    if (thisContainsOther) {
      var containedVert = otherVerts.stream().filter(this::contains).findFirst().get();

      var xNeighbor = getXNeighbor(containedVert, otherVerts);
      Integer xMin;
      Integer xMax;
      if (xNeighbor.getX() > containedVert.getX()) {
        xMin = containedVert.getX();
        xMax = Math.min(xNeighbor.getX(), this.xMax);
      } else {
        xMin = Math.max(xNeighbor.getX(), this.xMin);
        xMax = containedVert.getX();
      }


      var neighbor = getYNeighbor(containedVert, otherVerts);
      Integer yMin;
      Integer yMax;
      if (neighbor.getY() > containedVert.getY()) {
        yMin = containedVert.getY();
        yMax = Math.min(neighbor.getY(), this.yMax);
      } else {
        yMin = Math.max(neighbor.getY(), this.yMin);
        yMax = containedVert.getY();
      }

      var zNeighbor = getZNeighbor(containedVert, otherVerts);
      Integer zMin;
      Integer zMax;
      if (zNeighbor.getZ() > containedVert.getZ()) {
        zMin = containedVert.getZ();
        zMax = Math.min(zNeighbor.getZ(), this.zMax);
      } else {
        zMin = Math.max(zNeighbor.getZ(), this.zMin);
        zMax = containedVert.getZ();
      }

      var parents = new HashSet<String>();
      parents.addAll(this.getParents());
      parents.addAll(other.getParents());

      var ret = new Cuboid(this.id + "|" + other.id, parents, isOn, xMin, xMax, yMin, yMax, zMin, zMax);
//      this.addSubCuboid(ret);
//      other.addSubCuboid(ret);
      return ret;
    } else if (otherContainsThis) {
      return other.overlap(this, isOn);
    } else {
      var edgeOverlap = this.checkEdges(other, isOn);
      return edgeOverlap == null ? other.checkEdges(this, isOn) : edgeOverlap;
    }
  }

  private Cuboid checkEdges(Cuboid other, Boolean isOn) {
    var verts = other.getVertices();
    
    Set<Integer> xs = new HashSet<>();
    Set<Integer> ys = new HashSet<>();
    Set<Integer> zs = new HashSet<>();
    
    // x edges
    for (Coordinate vert : verts) {
      var xNeighbor = getXNeighbor(vert, verts);
      var crossesXMax = Math.max(vert.getX(), xNeighbor.getX()) >= this.xMax && Math.min(vert.getX(), xNeighbor.getX()) <= this.xMax;
      var crossesXMin = Math.max(vert.getX(), xNeighbor.getX()) >= this.xMin && Math.min(vert.getX(), xNeighbor.getX()) <= this.xMin;
      var edgeContained = (vert.getY() >= this.yMin && vert.getY() <= this.yMax)
          && (vert.getZ() >= this.zMin && vert.getZ() <= this.zMax)
          && (crossesXMax || crossesXMin);
      if (edgeContained) {
        ys.add(vert.getY());
        zs.add(vert.getZ());
        if (crossesXMax) {
          xs.add(this.xMax);
        } else {
          xs.add(Math.max(vert.getX(), xNeighbor.getX()));
        }
        
        if (crossesXMin) {
          xs.add(this.xMin);
        } else {
          xs.add(Math.min(vert.getX(), xNeighbor.getX()));
        }
      }
    }
    

    // y edges
    for (Coordinate vert : verts) {
      var neighbor = getYNeighbor(vert, verts);
      var crossesYMax = Math.max(vert.getY(), neighbor.getY()) >= this.yMax && Math.min(vert.getY(), neighbor.getY()) <= this.yMax;
      var crossesYMin = Math.max(vert.getY(), neighbor.getY()) >= this.yMin && Math.min(vert.getY(), neighbor.getY()) <= this.yMin;
      var edgeContained = (vert.getX() >= this.xMin && vert.getX() <= this.xMax)
          && (vert.getZ() >= this.zMin && vert.getZ() <= this.zMax)
          && (crossesYMax || crossesYMin);
      if (edgeContained) {
        xs.add(vert.getX());
        zs.add(vert.getZ());
        if (crossesYMax) {
          ys.add(this.yMax);
        } else {
          ys.add(Math.max(vert.getY(), neighbor.getY()));
        }

        if (crossesYMin) {
          ys.add(this.yMin);
        } else {
          ys.add(Math.min(vert.getY(), neighbor.getY()));
        }
      }
    }

    // z edges
    for (Coordinate vert : verts) {
      var neighbor = getZNeighbor(vert, verts);
      var crossesZMax = Math.max(vert.getZ(), neighbor.getZ()) >= this.zMax && Math.min(vert.getZ(), neighbor.getZ()) <= this.zMax;
      var crossesZMin = Math.max(vert.getZ(), neighbor.getZ()) >= this.zMin && Math.min(vert.getZ(), neighbor.getZ()) <= this.zMin;
      var edgeContained = (vert.getX() >= this.xMin && vert.getX() <= this.xMax)
          && (vert.getY() >= this.yMin && vert.getY() <= this.yMax)
          && (crossesZMax || crossesZMin);
      if (edgeContained) {
        xs.add(vert.getX());
        ys.add(vert.getY());
        if (crossesZMax) {
          zs.add(this.zMax);
        } else {
          zs.add(Math.max(vert.getZ(), neighbor.getZ()));
        }

        if (crossesZMin) {
          zs.add(this.zMin);
        } else {
          zs.add(Math.min(vert.getZ(), neighbor.getZ()));
        }
      }
    }

    try {
      if (xs.size() == 1) {
        var x = xs.toArray()[0];
        if (x == other.xMin && x == other.xMax) {
          // do nothing
        } else if (x == other.xMin) {
          xs.add(this.xMax);
        } else {
          xs.add(this.xMin);
        }
      }

      if (ys.size() == 1) {
        var y = ys.toArray()[0];
        if (y == other.yMin && y == other.yMax) {
          // do nothing
        } else if (y == other.yMin) {
          ys.add(this.yMax);
        } else {
          ys.add(this.yMin);
        }
      }

      if (zs.size() == 1) {
        var z = zs.toArray()[0];
        if (z == other.zMin && z == other.zMax) {
          // do nothing
        } else if (z == other.zMin) {
          zs.add(this.zMax);
        } else {
          zs.add(this.zMin);
        }
      }

      Integer xMin = xs.stream().min(Integer::compareTo).get();
      Integer xMax = xs.stream().max(Integer::compareTo).get();
      Integer yMin = ys.stream().min(Integer::compareTo).get();
      Integer yMax = ys.stream().max(Integer::compareTo).get();
      Integer zMin = zs.stream().min(Integer::compareTo).get();
      Integer zMax = zs.stream().max(Integer::compareTo).get();

      var parents = new HashSet<String>();
      parents.addAll(this.getParents());
      parents.addAll(other.getParents());
      return new Cuboid(this.id + "|" + other.id, parents, isOn, xMin, xMax, yMin, yMax, zMin, zMax);
    } catch (Exception ex) {
      return null;
    }
  }

  private Coordinate getXNeighbor(Coordinate thisCoord, List<Coordinate> all) {
    return all.stream().filter(vert -> !vert.getX().equals(thisCoord.getX()) && vert.getY().equals(thisCoord.getY()) && vert.getZ().equals(thisCoord.getZ())).findFirst().orElse(thisCoord);
  }

  private Coordinate getYNeighbor(Coordinate thisCoord, List<Coordinate> all) {
    return all.stream().filter(vert -> vert.getX().equals(thisCoord.getX()) && !vert.getY().equals(thisCoord.getY()) && vert.getZ().equals(thisCoord.getZ())).findFirst().orElse(thisCoord);
  }

  private Coordinate getZNeighbor(Coordinate thisCoord, List<Coordinate> all) {
    return all.stream().filter(vert -> vert.getX().equals(thisCoord.getX()) && vert.getY().equals(thisCoord.getY()) && !vert.getZ().equals(thisCoord.getZ())).findFirst().orElse(thisCoord);
  }

  public List<Coordinate> getVertices() {
    return Arrays.asList(
        new Coordinate(xMin, yMin, zMin),
        new Coordinate(xMin, yMin, zMax),
        new Coordinate(xMin, yMax, zMin),
        new Coordinate(xMin, yMax, zMax),
        new Coordinate(xMax, yMin, zMin),
        new Coordinate(xMax, yMin, zMax),
        new Coordinate(xMax, yMax, zMin),
        new Coordinate(xMax, yMax, zMax)
    );
  }

  public boolean contains(Coordinate coordinate) {
    return coordinate.getX() >= xMin && coordinate.getX() <= xMax
        && coordinate.getY() >= yMin && coordinate.getY() <= yMax
        && coordinate.getZ() >= zMin && coordinate.getZ() <= zMax;
  }

//  private void addSubCuboid(Cuboid subCuboid) {
//    subCuboids.add(subCuboid);
//  }

  public Long getSize() {
    return ((long) (xMax - xMin) + 1) * ((long) (yMax - yMin) + 1) * ((long) (zMax - zMin) + 1);
  }

  public Set<Coordinate> getAllPixels() {
    var out = new HashSet<Coordinate>();
    for (var x=this.getXMin(); x<=this.getXMax(); x++) {
      for (var y=this.getYMin(); y<=this.getYMax(); y++) {
        for (var z=this.getZMin(); z<=this.getZMax(); z++) {
          out.add(new Coordinate(x,y,z));
        }
      }
    }
    return out;
  }

//  public Long getAdjustedSize() {
//    var amountToSubtract = getAllSubCuboids().stream().mapToLong(Cuboid::getAdjustedSize).sum();
//    return getSize() - amountToSubtract;
//  }

//  private Set<Cuboid> getAllSubCuboids() {
//    if (subCuboids.isEmpty()) {
//      return Collections.emptySet();
//    }
//    var subSubCuboids = subCuboids.stream()
//        .map(Cuboid::getAllSubCuboids)
//        .flatMap(Set::stream)
//        .collect(Collectors.toSet());
//    var ret = new HashSet<>(subCuboids);
//    ret.addAll(subSubCuboids);
//    return ret;
//  }


}

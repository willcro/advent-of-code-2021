package com.willcro.advent.day20;

import com.willcro.advent.day19.Day19;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Day20 {

  public static void main(String[] args) throws IOException {
    var in = Day19.class.getResourceAsStream("/day20main.txt");
    var inputString = new String(in.readAllBytes());
    var input = parseInput(inputString);

    drawImage(input.getImage());
    System.out.println();
    var processed = input.getImage();
    for (var i=0; i<25; i++) {
      processed = processImage(input.getAlgorithm(), processed, false);
      processed = processImage(input.getAlgorithm(), processed, true);
    }
    drawImage(processed);

    var litPixels = processed.stream().flatMap(List::stream).filter(it -> it).collect(Collectors.toList()).size();
    System.out.println(litPixels);

    return;
  }

  private static Day20Input parseInput(String input) {
    var firstLine = input.substring(0, input.indexOf('\n'));
    var algo = Arrays.stream(firstLine.split(""))
        .map(it -> it.equals("#"))
        .collect(Collectors.toList());

    var imageString = input.substring(input.indexOf('\n') + 2);
    var image = Arrays.stream(imageString.split("\n"))
        .map(line -> Arrays.stream(line.split("")).map(it -> it.equals("#")).collect(Collectors.toList()))
        .collect(Collectors.toList());

    return new Day20Input(algo, image);
  }

  private static List<List<Boolean>> processImage(List<Boolean> algo, List<List<Boolean>> image, Boolean backgroundLit) {

    if (backgroundLit) {
      image.add(0, lightLine(image.get(0).size()));
      image.add(0, lightLine(image.get(0).size()));
      image.add(lightLine(image.get(0).size()));
      image.add(lightLine(image.get(0).size()));
    } else {
      image.add(0, darkLine(image.get(0).size()));
      image.add(0, darkLine(image.get(0).size()));
      image.add(darkLine(image.get(0).size()));
      image.add(darkLine(image.get(0).size()));
    }

    var paddedImage = image.stream().map(line -> {
      line.add(0, backgroundLit);
      line.add(0, backgroundLit);
      line.add(backgroundLit);
      line.add(backgroundLit);
      return line;
    }).collect(Collectors.toList());

    var outImage = new ArrayList<List<Boolean>>();
    for (var y=1; y<paddedImage.size() - 1; y++) {
      var imageLine = paddedImage.get(y);
      var outLine = new ArrayList<Boolean>();
      for (var x=1; x<imageLine.size() - 1; x++) {
        var b1 = paddedImage.get(y-1).get(x-1) ? 256 : 0;
        var b2 = paddedImage.get(y-1).get(x) ? 128 : 0;
        var b3 = paddedImage.get(y-1).get(x+1) ? 64 : 0;
        var b4 = paddedImage.get(y).get(x-1) ? 32 : 0;
        var b5 = paddedImage.get(y).get(x) ? 16 : 0;
        var b6 = paddedImage.get(y).get(x+1) ? 8 : 0;
        var b7 = paddedImage.get(y+1).get(x-1) ? 4 : 0;
        var b8 = paddedImage.get(y+1).get(x) ? 2 : 0;
        var b9 = paddedImage.get(y+1).get(x+1) ? 1 : 0;
        var index = b1 + b2 + b3 + b4 + b5 + b6 + b7 + b8 + b9;
        var isLit = algo.get(index);
        outLine.add(isLit);
      }
      outImage.add(outLine);
    }
    return outImage;
  }

  private static void drawImage(List<List<Boolean>> image) {
    image.forEach(line -> {
      line.forEach(it -> {
        if (it) {
          System.out.print("#");
        } else {
          System.out.print(".");
        }
      });
      System.out.println();
    });
  }

  private static List<Boolean> darkLine(Integer length) {
    var ret = new ArrayList<Boolean>();
    for (var i=0; i<length; i++) {
      ret.add(false);
    }
    return ret;
  }

  private static List<Boolean> lightLine(Integer length) {
    var ret = new ArrayList<Boolean>();
    for (var i=0; i<length; i++) {
      ret.add(true);
    }
    return ret;
  }

}

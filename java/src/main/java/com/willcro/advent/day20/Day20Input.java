package com.willcro.advent.day20;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class Day20Input {

  private List<Boolean> algorithm;
  private List<List<Boolean>> image;

}

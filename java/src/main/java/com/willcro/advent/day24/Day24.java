package com.willcro.advent.day24;

import com.willcro.advent.day25.Day25;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Pattern;

public class Day24 {

  public static void main(String[] args) throws IOException {
    var in = Day25.class.getResourceAsStream("/day24main.txt");
    var inputString = new String(in.readAllBytes());
    var pattern = Pattern.compile("(\\w{3}) (\\w+) ?(.*)");
    var instructions = inputString.split("\n");

    Scanner scanner = new Scanner(System.in);
    while (true) {
      var monad = scanner.nextLine();
      var monadCursor = 0;

      var vars = new HashMap<>(Map.of(
          "w", 0L,
          "x", 0L,
          "y", 0L,
          "z", 0L
      ));

      var line = 1;

      for (String instruction : instructions) {
        var matcher = pattern.matcher(instruction);
        if (!matcher.matches()) {
          throw new RuntimeException("how?");
        }

        var type = matcher.group(1);
        var a = matcher.group(2);
        var b = matcher.group(3);

        Long bValue;

        try {
          bValue = Long.parseLong(b);
        } catch (Exception ex) {
          bValue = vars.get(b);
        }

        Long out;

        if (type.equals("eql")) {
          out = vars.get(a).equals(bValue) ? 1L : 0L;
        } else if (type.equals("add")) {
          out = vars.get(a) + bValue;
        } else if (type.equals("mod")) {
          out = vars.get(a) % bValue;
        } else if (type.equals("div")) {
          out = vars.get(a) / bValue;
        } else if (type.equals("mul")) {
          out = vars.get(a) * bValue;
        } else if (type.equals("inp")) {
          System.out.println("After digit " + (monadCursor) + ": z=" + vars.get("z"));
          out = Long.valueOf(monad.substring(monadCursor, monadCursor + 1));
          monadCursor++;
        } else {
          throw new RuntimeException("ex");
        }

        vars.put(a, out);

        line++;
      }
      System.out.println("z=" + vars.get("z"));
    }


  }

}

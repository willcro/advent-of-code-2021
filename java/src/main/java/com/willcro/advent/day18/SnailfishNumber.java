package com.willcro.advent.day18;

import lombok.Data;

@Data
public abstract class SnailfishNumber {

  protected CompoundSnailfishNumber parent;
  protected CompoundSide side;

  protected SnailfishNumber(CompoundSnailfishNumber parent, CompoundSide side) {
    this.parent = parent;
    this.side = side;
  }

  public abstract Long getMagnitude();

  public abstract boolean reduce();

  public abstract boolean explode();

  public abstract boolean split();

  public Integer getNestingness() {
    if (parent == null) {
      return 0;
    } else {
      return parent.getNestingness() + 1;
    }
  }

  public SnailfishNumber plus(SnailfishNumber other) {
    var parent = new CompoundSnailfishNumber(null, null, null, null);
    this.side = CompoundSide.LEFT;
    this.parent = parent;
    other.side = CompoundSide.RIGHT;
    other.parent = parent;
    parent.setChild(CompoundSide.LEFT, this);
    parent.setChild(CompoundSide.RIGHT, other);
    return parent;
  }

}

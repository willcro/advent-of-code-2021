package com.willcro.advent.day18;

public class CompoundSnailfishNumber extends SnailfishNumber {

  private SnailfishNumber left;
  private SnailfishNumber right;

  protected CompoundSnailfishNumber(CompoundSnailfishNumber parent, CompoundSide side,
      SnailfishNumber left, SnailfishNumber right) {
    super(parent, side);
    this.left = left;
    this.right = right;
  }

  @Override
  public Long getMagnitude() {
    return 3 * left.getMagnitude() + 2 * right.getMagnitude();
  }

  @Override
  public boolean reduce() {
    if (this.explode()) {
      return this.reduce();
    }
    // nothing exploded, try splitting
    if (this.split()) {
      return this.reduce();
    }

    return false;
  }

  @Override
  public boolean explode() {
    if (this.getNestingness() >= 4 && left instanceof ConstantSnailfishNumber && right instanceof ConstantSnailfishNumber) {
      this.parent.setChild(this.side, new ConstantSnailfishNumber(0L, this.parent, this.side));
      var leftNeighbor = findClosestConstantNeighbor(CompoundSide.LEFT);
      var rightNeighbor = findClosestConstantNeighbor(CompoundSide.RIGHT);
      if (leftNeighbor != null) {
        leftNeighbor.setValue(leftNeighbor.getValue() + ((ConstantSnailfishNumber) left).getValue());
      }
      if (rightNeighbor != null) {
        rightNeighbor.setValue(rightNeighbor.getValue() + ((ConstantSnailfishNumber) right).getValue());
      }
      return true;
    } else {
      if (left.explode()) {
        return true;
      }
      if (right.explode()) {
        return true;
      }
    }
    return false;
  }

  @Override
  public boolean split() {
    if (left.split()) {
      return true;
    }
    if (right.split()) {
      return true;
    }
    return false;
  }

  public void setChild(CompoundSide side, SnailfishNumber number) {
    if (side.equals(CompoundSide.LEFT)) {
      this.left = number;
    } else {
      this.right = number;
    }
  }

  protected ConstantSnailfishNumber findClosestConstantNeighbor(CompoundSide side) {
    if (parent == null) {
      return null;
    }

    if (side.equals(CompoundSide.LEFT)) {
      if (this.side == CompoundSide.RIGHT) {
        var number = this.parent.left;
        while (number instanceof CompoundSnailfishNumber) {
          number = ((CompoundSnailfishNumber) number).right;
        }
        return (ConstantSnailfishNumber) number;
      } else {
        return parent.findClosestConstantNeighbor(side);
      }
    } else {
      if (this.side == CompoundSide.LEFT) {
        var number = this.parent.right;
        while (number instanceof CompoundSnailfishNumber) {
          number = ((CompoundSnailfishNumber) number).left;
        }
        return (ConstantSnailfishNumber) number;
      } else {
        return parent.findClosestConstantNeighbor(side);
      }
    }
  }

  @Override
  public String toString() {
    return "[" + left.toString() + "," + right.toString() + "]";
  }
}

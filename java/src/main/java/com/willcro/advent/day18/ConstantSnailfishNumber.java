package com.willcro.advent.day18;

import lombok.Getter;
import lombok.Setter;

public class ConstantSnailfishNumber extends SnailfishNumber {

  @Getter
  @Setter
  private Long value;

  public ConstantSnailfishNumber(Long value, CompoundSnailfishNumber parent, CompoundSide side) {
    super(parent, side);
    this.value = value;
  }

  @Override
  public Long getMagnitude() {
    return value;
  }

  @Override
  public boolean reduce() {
    return false;
  }

  @Override
  public boolean explode() {
    return false;
  }

  @Override
  public boolean split() {
    if (value > 9) {
      var left = new ConstantSnailfishNumber((long) Math.floor(this.value / 2.0), null, CompoundSide.LEFT);
      var right = new ConstantSnailfishNumber((long) Math.ceil(this.value / 2.0), null, CompoundSide.RIGHT);
      var newValue = new CompoundSnailfishNumber(this.parent, this.side, left, right);
      left.parent = newValue;
      right.parent = newValue;
      this.parent.setChild(side, newValue);
      return true;
    }
    return false;
  }

  @Override
  public String toString() {
    return String.valueOf(value);
  }
}

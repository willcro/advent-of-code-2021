package com.willcro.advent.day18;

public enum CompoundSide {
  LEFT,
  RIGHT
}

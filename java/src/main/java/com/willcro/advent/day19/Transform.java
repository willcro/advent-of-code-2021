package com.willcro.advent.day19;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@AllArgsConstructor
@Data
public class Transform {

//  @EqualsAndHashCode.Exclude
  private Rotation rotation;
  private Integer xTranslation;
  private Integer yTranslation;
  private Integer zTranslation;

}

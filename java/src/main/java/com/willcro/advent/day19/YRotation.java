package com.willcro.advent.day19;

public class YRotation implements Rotation {


  @Override
  public Beacon transform(Beacon beacon) {
    var x = beacon.getZ();
    var y = beacon.getY();
    var z = -beacon.getX();

    return new Beacon(x, y, z);
  }
}

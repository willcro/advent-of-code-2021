package com.willcro.advent.day19;

import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import lombok.AllArgsConstructor;

public interface Rotation {

  public static Rotation X = new XRotation();
  public static Rotation Y = new YRotation();
  public static Rotation Z = new ZRotation();

  public static List<Rotation> ALL_COMPOUND_ROTATIONS = Arrays.asList(
      // Rightside up
      new CompoundRotation(Arrays.asList()),
      new CompoundRotation(Arrays.asList(X)),
      new CompoundRotation(Arrays.asList(X,X)),
      new CompoundRotation(Arrays.asList(X,X,X)),
      // Upside down
      new CompoundRotation(Arrays.asList(Y,Y)),
      new CompoundRotation(Arrays.asList(X,Y,Y)),
      new CompoundRotation(Arrays.asList(X,X,Y,Y)),
      new CompoundRotation(Arrays.asList(X,X,X,Y,Y)),

      // On its side
      new CompoundRotation(Arrays.asList(Y)),
      new CompoundRotation(Arrays.asList(X,Y)),
      new CompoundRotation(Arrays.asList(X,X,Y)),
      new CompoundRotation(Arrays.asList(X,X,X,Y)),
      new CompoundRotation(Arrays.asList(Y,Y,Y)),
      new CompoundRotation(Arrays.asList(X,Y,Y,Y)),
      new CompoundRotation(Arrays.asList(X,X,Y,Y,Y)),
      new CompoundRotation(Arrays.asList(X,X,X,Y,Y,Y)),
      new CompoundRotation(Arrays.asList(Z)),
      new CompoundRotation(Arrays.asList(X,Z)),
      new CompoundRotation(Arrays.asList(X,X,Z)),
      new CompoundRotation(Arrays.asList(X,X,X,Z)),
      new CompoundRotation(Arrays.asList(Z,Z,Z)),
      new CompoundRotation(Arrays.asList(X,Z,Z,Z)),
      new CompoundRotation(Arrays.asList(X,X,Z,Z,Z)),
      new CompoundRotation(Arrays.asList(X,X,X,Z,Z,Z))
  );

  Beacon transform(Beacon beacon);

}

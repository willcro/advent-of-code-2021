package com.willcro.advent.day19;

public class ZRotation implements Rotation {


  @Override
  public Beacon transform(Beacon beacon) {
    var x = -beacon.getY();
    var y = beacon.getX();
    var z = beacon.getZ();

    return new Beacon(x, y, z);
  }
}

package com.willcro.advent.day19;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Day19 {

  public static void main(String[] args) throws IOException {
    var in = Day19.class.getResourceAsStream("/day19main.txt");
    var input = new String(in.readAllBytes());
    var scanners = parseInput(input);
    scanners.get(0).overlaps(scanners.get(1));

    var pathTo0Space = new HashMap<Integer, List<Transform>>();
    pathTo0Space.put(0, Collections.emptyList());

    var overlaps = scanners.stream().flatMap(thisScanner -> scanners.stream()
        .filter(otherScanner -> !thisScanner.getId().equals(otherScanner.getId()))
        .map(thisScanner::overlaps)
        .filter(Optional::isPresent))
        .map(Optional::get)
        .collect(Collectors.toList());

    while(pathTo0Space.keySet().size() < scanners.size()) {
      for (Overlap overlap : overlaps) {
        if (!pathTo0Space.containsKey(overlap.getScanner2().getId()) && pathTo0Space.containsKey(overlap.getScanner1().getId())) {
          var path = new ArrayList<>(pathTo0Space.get(overlap.getScanner1().getId()));
          path.add(0, overlap.getTransform());
          pathTo0Space.put(overlap.getScanner2().getId(), path);
        }
      }
    }

    var beacons = scanners.stream().flatMap(scanner -> {
      var transformers = pathTo0Space.get(scanner.getId());
      return scanner.getBeacons().stream().map(beacon -> {
        var out = beacon;
        for (Transform transform : transformers) {
          out = out.transform(transform);
        }
        return out;
      });
    }).collect(Collectors.toSet());

    // part 2
    var scannerCoords = scanners.stream().map(scanner -> {
        var transformers = pathTo0Space.get(scanner.getId());
        var out = new Beacon(0,0,0);
        for (Transform transform : transformers) {
          out = out.transform(transform);
        }
        return out;
      }).collect(Collectors.toList());

    var maxMathattanDistance = scannerCoords.stream().flatMapToInt(s -> scannerCoords.stream().mapToInt(s::manhattanDistance)).max();

    return;
  }

  private static List<Scanner> parseInput(String input) {
    return Arrays.stream(input.split("\n\n")).map(Day19::parseScanner).collect(Collectors.toList());
  }

  private static Scanner parseScanner(String scanner) {
    var firstLine = scanner.substring(0, scanner.indexOf('\n'));
    var rest = scanner.substring(scanner.indexOf('\n') + 1);
    var pattern = Pattern.compile("--- scanner (\\d+) ---");
    var matcher = pattern.matcher(firstLine);
    if (!matcher.matches()) {
      throw new RuntimeException("Parse error");
    }
    var id = Integer.parseInt(matcher.group(1));
    var ret = new Scanner();
    ret.setId(id);

    // parse beacons
    var beacons = Arrays.stream(rest.split("\n")).map(Beacon::new).collect(Collectors.toList());
    ret.setBeacons(beacons);

    return ret;
  }

}

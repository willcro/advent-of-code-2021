package com.willcro.advent.day19;

import java.util.List;

public class CompoundRotation implements Rotation {

  private final List<Rotation> rotations;

  public CompoundRotation(List<Rotation> rotations) {
    this.rotations = rotations;
  }

  @Override
  public Beacon transform(Beacon beacon) {
    var out = beacon;
    for (Rotation rotation : rotations) {
      out = rotation.transform(out);
    }
    return out;
  }
}

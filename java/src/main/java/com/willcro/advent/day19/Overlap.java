package com.willcro.advent.day19;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Overlap {

  private Scanner scanner1;
  private Scanner scanner2;
  private Transform transform;

}

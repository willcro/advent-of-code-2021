package com.willcro.advent.day19;

import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.Data;

@Data
public class Scanner {

  private Integer id;
  private List<Beacon> beacons;
  private Coordinates coordinates;

  public Optional<Overlap> overlaps(Scanner other) {
    var counts = other.beacons.stream().flatMap(otherB ->
            this.beacons.stream().flatMap(thisB ->
                otherB.getPossibleTransforms(thisB).stream()))
        .collect(Collectors.groupingBy(it -> it, Collectors.counting()));

    return counts.entrySet().stream()
        .filter(entry -> entry.getValue() >= 12)
        .map(Entry::getKey)
        .map(transform -> new Overlap(this, other, transform))
        .findFirst();
  }

}

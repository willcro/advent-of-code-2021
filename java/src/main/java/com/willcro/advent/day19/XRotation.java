package com.willcro.advent.day19;

public class XRotation implements Rotation {

  @Override
  public Beacon transform(Beacon beacon) {
    var x = beacon.getX();
    var y = -beacon.getZ();
    var z = beacon.getY();

    return new Beacon(x, y, z);
  }
}

package com.willcro.advent.day19;

import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class Beacon {

  private static final Pattern COORD_PATTERN = Pattern.compile("(-?[0-9]+),(-?[0-9]+),(-?[0-9]+)");

  public Beacon(String rawInput) {
    var matcher = COORD_PATTERN.matcher(rawInput);
    if (!matcher.matches()) {
      throw new RuntimeException("Bad input " + rawInput);
    }
    this.x = Integer.parseInt(matcher.group(1));
    this.y = Integer.parseInt(matcher.group(2));
    this.z = Integer.parseInt(matcher.group(3));
  }

  private Integer x;
  private Integer y;
  private Integer z;

  public Beacon transform(Transform transform) {
    var thisRotated = transform.getRotation().transform(this);
    var newx = thisRotated.x + transform.getXTranslation();
    var newy = thisRotated.y + transform.getYTranslation();
    var newz = thisRotated.z + transform.getZTranslation();
    return new Beacon(newx, newy, newz);
  }

  public List<Transform> getPossibleTransforms(Beacon other) {
    return Rotation.ALL_COMPOUND_ROTATIONS.stream()
        .map(r -> {
          var thisRotated = r.transform(this);
          var xDiff = other.x - thisRotated.x;
          var yDiff = other.y - thisRotated.y;
          var zDiff = other.z - thisRotated.z;
          return new Transform(r, xDiff, yDiff, zDiff);
        }).collect(Collectors.toList());
  }

  public Integer manhattanDistance(Beacon other) {
    return Math.abs(this.x - other.x) + Math.abs(this.y - other.y) + Math.abs(this.z - other.z);
  }

}

package com.willcro.advent.day16;

public class LessThanPacket extends OperatorPacket {

  public LessThanPacket(Integer version) {
    super(version, 6);
  }

  @Override
  public Long getValue() {
    return subPackets.get(0).getValue() < subPackets.get(1).getValue() ? 1L : 0L;
  }

}

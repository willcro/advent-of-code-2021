package com.willcro.advent.day16;

import java.util.ArrayList;
import java.util.List;

public abstract class OperatorPacket extends Packet {

  protected List<Packet> subPackets;

  public OperatorPacket(Integer version, Integer type) {
    super(version, type);
  }

  @Override
  public String readData(String data) {
    var readType = data.charAt(0);
    if (readType == '0') {
      var bitsToRead = Integer.valueOf(data.substring(1, 16), 2);
      return readDataBits(data.substring(16), bitsToRead);
    } else if (readType == '1') {
      var packetsToRead = Integer.valueOf(data.substring(1, 12), 2);
      return readDataPackets(data.substring(12), packetsToRead);
    } else {
      throw new RuntimeException("Unrecognized read type " + readType);
    }
  }

  @Override
  public Long getVersionSum() {
    return getVersion() + subPackets.stream().mapToLong(Packet::getVersionSum).sum();
  }

  private String readDataBits(String data, Integer bitsToRead) {
    var dataToRead = data.substring(0, bitsToRead);
    data = data.substring(bitsToRead);
    this.subPackets = new ArrayList<>();

    while (!dataToRead.isEmpty()) {
      var header = dataToRead.substring(0, 6);
      var packet = PacketFactory.createPacket(header);
      dataToRead = packet.readData(dataToRead.substring(6));
      this.subPackets.add(packet);
    }

    return data;
  }

  private String readDataPackets(String data, Integer packetsToRead) {
    this.subPackets = new ArrayList<>();

    for (var p=0; p<packetsToRead; p++) {
      var header = data.substring(0, 6);
      var packet = PacketFactory.createPacket(header);
      data = packet.readData(data.substring(6));
      this.subPackets.add(packet);
    }

    return data;
  }

  @Override
  public String toString() {
    return String.valueOf(getValue());
  }
}

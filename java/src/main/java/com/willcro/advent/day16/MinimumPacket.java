package com.willcro.advent.day16;

import java.util.Comparator;

public class MinimumPacket extends OperatorPacket {

  public MinimumPacket(Integer version) {
    super(version, 2);
  }

  @Override
  public Long getValue() {
    return this.subPackets.stream().min(Comparator.comparing(Packet::getValue)).get().getValue();
  }

}

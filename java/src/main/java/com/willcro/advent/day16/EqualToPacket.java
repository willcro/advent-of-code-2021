package com.willcro.advent.day16;

public class EqualToPacket extends OperatorPacket {

  public EqualToPacket(Integer version) {
    super(version, 7);
  }

  @Override
  public Long getValue() {
    return subPackets.get(0).getValue().equals(subPackets.get(1).getValue()) ? 1L : 0L;
  }

}

package com.willcro.advent.day16;

import java.util.ArrayList;
import java.util.List;

public class PacketFactory {

  /**
   * Creates an empty packet with the correct type but no data
   * @param header binary header
   * @return new empty packet
   */
  public static Packet createPacket(String header) {
    var version = Integer.valueOf(header.substring(0, 3), 2);
    var type = Integer.valueOf(header.substring(3, 6), 2);
    switch (type) {
      case 0: return new SumPacket(version);
      case 1: return new ProductPacket(version);
      case 2: return new MinimumPacket(version);
      case 3: return new MaximumPacket(version);
      case 4: return new LiteralPacket(version);
      case 5: return new GreaterThanPacket(version);
      case 6: return new LessThanPacket(version);
      case 7: return new EqualToPacket(version);
      default: throw new RuntimeException("Unknown packet type " + type);
    }
  }

  /**
   * Turns binary data into a list of Packets
   * @param data binary string
   * @return list of all packets in data
   */
  public static List<Packet> readPackets(String data) {
    List<Packet> packets = new ArrayList<>();

    while (data.length() > 7) {
      var header = data.substring(0, 6);
      var packet = createPacket(header);
      data = packet.readData(data.substring(6));
      packets.add(packet);
    }

    return packets;
  }

}

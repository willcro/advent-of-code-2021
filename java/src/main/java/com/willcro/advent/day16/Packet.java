package com.willcro.advent.day16;

import java.util.ArrayList;
import java.util.List;
import lombok.Data;

@Data
public abstract class Packet {

  protected Packet(Integer version, Integer type) {
    this.version = version;
    this.type = type;
  }

  private final Integer version;
  private final Integer type;

  /**
   * Read data into the packet
   * @param data Raw binary input
   * @return remaining bits
   */
  abstract public String readData(String data);

  abstract public Long getVersionSum();

  abstract public Long getValue();

}

package com.willcro.advent.day16;

import java.util.Comparator;

public class MaximumPacket extends OperatorPacket {

  public MaximumPacket(Integer version) {
    super(version, 3);
  }

  @Override
  public Long getValue() {
    return this.subPackets.stream().max(Comparator.comparing(Packet::getValue)).get().getValue();
  }
}

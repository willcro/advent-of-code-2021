package com.willcro.advent.day16;

public class SumPacket extends OperatorPacket {

  public SumPacket(Integer version) {
    super(version, 0);
  }

  @Override
  public Long getValue() {
    return this.subPackets.stream().mapToLong(Packet::getValue).sum();
  }
}

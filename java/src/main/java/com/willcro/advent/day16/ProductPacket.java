package com.willcro.advent.day16;

public class ProductPacket extends OperatorPacket {

  public ProductPacket(Integer version) {
    super(version, 1);
  }

  @Override
  public Long getValue() {
    return this.subPackets.stream().mapToLong(Packet::getValue).reduce(1, (a,b) -> a * b);
  }
}

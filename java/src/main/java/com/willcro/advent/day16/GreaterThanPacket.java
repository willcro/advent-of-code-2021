package com.willcro.advent.day16;

public class GreaterThanPacket extends OperatorPacket {

  public GreaterThanPacket(Integer version) {
    super(version, 5);
  }


  @Override
  public Long getValue() {
    return subPackets.get(0).getValue() > subPackets.get(1).getValue() ? 1L : 0L;
  }
}

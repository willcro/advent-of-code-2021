package com.willcro.advent.day16;

import lombok.Data;

public class LiteralPacket extends Packet {

  private Long value;

  public LiteralPacket(Integer version) {
    super(version, 4);
  }

  @Override
  public String readData(String data) {
    if (this.value != null) {
      throw new RuntimeException("This packet already contains data");
    }

    var done = false;
    var valueBits = new StringBuilder();

    while (!done) {
      // read 5 bits
      var bits = data.substring(0, 5);
      data = data.substring(5);

      if (bits.charAt(0) == '0') {
        done = true;
      }

      valueBits.append(bits.substring(1));
    }

    this.value = Long.parseLong(valueBits.toString(), 2);

    return data;
  }

  @Override
  public Long getVersionSum() {
    return Long.valueOf(getVersion());
  }

  @Override
  public Long getValue() {
    return value;
  }

  @Override
  public String toString() {
    return String.valueOf(getValue());
  }
}

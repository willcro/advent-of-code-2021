package com.willcro.advent.day12;

import java.util.Collections;
import java.util.List;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(of = "id")
public class EndNode implements Node {

  private final String id = "end";

  @Override
  public List<Node> getNextSteps() {
    return Collections.emptyList();
  }

  @Override
  public void addNextStep(Node node) {
    // do nothing
  }

  @Override
  public String getId() {
    return "end";
  }

  public String toString() {
    return "end";
  }
}

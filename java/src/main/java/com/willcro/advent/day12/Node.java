package com.willcro.advent.day12;

import java.util.List;
import java.util.Set;

public interface Node {

  List<Node> getNextSteps();
  void addNextStep(Node node);
  String getId();

}

package com.willcro.advent.day12;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.regex.Pattern;
import lombok.Data;

@Data
public class Day12Graph {

  private StartNode start;
  private EndNode end;

  public Set<Journey> getAllJourneys(Integer searches) {
    var uniques = new HashSet<Journey>();
    for (var i=0; i<searches; i++) {
      try {
        uniques.add(createRandomJourney());
      } catch (DeadEndException ex) {
        // do nothing
      }
    }
    return uniques;
  }

  public int depthFirstSearch() {
    var count = 0;
    var journey = new Journey();
    journey.visitNode(start);

    try {
      while (true) {
        while (!journey.isFinished()) {
          var nextSteps = journey.getNextSteps();
          if (nextSteps.isEmpty()) {
            journey.backtrack();
            count++;
          } else {
            journey.visitNode(nextSteps.get(0));
          }
        }

        count++;
        journey.backtrack();
      }
    } catch (DeadEndException ex) {
      return count;
    }
  }

  public Journey createRandomJourney() throws DeadEndException {
    var journey = new Journey();
    journey.visitNode(start);
    while (!journey.isFinished()) {
      var nextSteps = journey.getNextSteps();
      if (nextSteps.isEmpty()) {
        throw new DeadEndException("Reached a dead end");
      }
      var nextStep = nextSteps.get(new Random().nextInt(nextSteps.size()));
      journey.visitNode(nextStep);
    }
    return journey;
  }

  public static Day12Graph createGraph(List<String> lines) {
    var nodes = new HashMap<String, Node>();
    var startNode = new StartNode();
    var endNode = new EndNode();
    nodes.put("start", startNode);
    nodes.put("end", endNode);
    for (String line : lines) {
      var pattern = Pattern.compile("(\\w+)-(\\w+)");
      var matcher = pattern.matcher(line);
      if (matcher.matches()) {
        var nodeId1 = matcher.group(1);
        nodes.putIfAbsent(nodeId1, idToNode(nodeId1));
        var node1 = nodes.get(nodeId1);
        var nodeId2 = matcher.group(2);
        nodes.putIfAbsent(nodeId2, idToNode(nodeId2));
        var node2 = nodes.get(nodeId2);

        node1.addNextStep(node2);
        node2.addNextStep(node1);
      }
    }

    var graph = new Day12Graph();
    graph.setStart(startNode);
    graph.setEnd(endNode);
    return graph;
  }

  private static Node idToNode(String id) {
    if (id.equalsIgnoreCase("start")) {
      return new StartNode();
    } else if (id.equalsIgnoreCase("end")) {
      return new EndNode();
    } else if (id.equals(id.toLowerCase())) {
      return new SmallCaveNode(id);
    } else if (id.equals(id.toUpperCase())) {
      return new BigCaveNode(id);
    } else {
      throw new RuntimeException("What have you done fool");
    }
  }

}

package com.willcro.advent.day12;

public class DeadEndException extends Exception {
  public DeadEndException(String msg) {
    super(msg);
  }
}

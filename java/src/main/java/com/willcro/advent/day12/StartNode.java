package com.willcro.advent.day12;

import java.util.ArrayList;
import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(of = "id")
@Data
public class StartNode implements Node {

  private final String id = "start";
  private List<Node> nextSteps = new ArrayList<>();

  @Override
  public void addNextStep(Node node) {
    if (!nextSteps.contains(node)) {
      nextSteps.add(node);
    }
  }

  @Override
  public String getId() {
    return "start";
  }

  public String toString() {
    return "start";
  }


}

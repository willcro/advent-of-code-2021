package com.willcro.advent.day12;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Stack;
import java.util.stream.Collectors;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class Journey {

  private Stack<Node> nodes = new Stack<>();
  private boolean finished = false;

  public boolean nextNodeValid(Node node) {
    if (finished) {
      return false;
    } else if (node instanceof StartNode) {
      return nodes.isEmpty();
    } else if (node instanceof EndNode) {
      // journey must contain at least one SmallCaveNode
//      return nodes.stream().anyMatch(n -> n instanceof SmallCaveNode);
      return true;
    } else if (node instanceof SmallCaveNode) {
      // journey can't contain the same small cave twice

      // change from part 1
//      return nodes.stream().noneMatch(n -> n.equals(node));
      var counts = nodes.stream()
          .filter(n -> n instanceof SmallCaveNode)
          .collect(Collectors.groupingBy(Node::getId, Collectors.counting()));
      if (!counts.containsKey(node.getId())) {
        return true;
      } else {
        var thisCount = counts.get(node.getId());
        if (thisCount > 1) {
          return false;
        }
        return counts.values().stream().noneMatch(it -> it > 1);
      }
    } else if (node instanceof BigCaveNode) {
      return true;
    } else {
      // includes start node
      return false;
    }
  }

  public void visitNode(Node node) {
    if (!nextNodeValid(node)) {
      throw new RuntimeException("Node is not valid for this journey");
    } else {
      nodes.push(node);
    }
    if (node instanceof EndNode) {
      finished = true;
    }
  }

  public boolean isFinished() {
    return finished;
  }

  public Node currentNode() {
    return nodes.get(nodes.size() - 1);
  }

  public String toString() {
    return nodes.stream().map(it -> it.getId()).collect(Collectors.joining(","));
  }

  public List<Node> getNextSteps() throws DeadEndException {
    if (nodes.isEmpty()) {
      throw new DeadEndException("Our back is against the wall");
    }
    var currentNode = nodes.peek();
    return currentNode.getNextSteps().stream()
        .filter(this::nextNodeValid)
        .sorted(Comparator.comparing(Node::getId))
        .collect(Collectors.toList());
  }

  public void backtrack() throws DeadEndException {
    if (nodes.isEmpty()) {
      throw new DeadEndException("Our back is against the wall");
    }

    finished = false;
    var popped = nodes.pop();
    var nextSteps = getNextSteps();
    var index = nextSteps.indexOf(popped);

    // if this was the last option
    if (index == nextSteps.size() - 1) {
      backtrack();
    } else {
      visitNode(nextSteps.get(index + 1));
    }
  }
}

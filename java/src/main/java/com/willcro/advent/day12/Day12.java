package com.willcro.advent.day12;

import java.util.Arrays;
import java.util.List;

public class Day12 {

  public static List<String> testInput = Arrays.asList(
      "fs-end",
      "he-DX",
      "fs-he",
      "start-DX",
      "pj-DX",
      "end-zg",
      "zg-sl",
      "zg-pj",
      "pj-he",
      "RW-he",
      "fs-DX",
      "pj-RW",
      "zg-RW",
      "start-pj",
      "he-WI",
      "zg-he",
      "pj-fs",
      "start-RW"
  );

  public static List<String> testInput2 = Arrays.asList(
      "dc-end",
      "HN-start",
      "start-kj",
      "dc-start",
      "dc-HN",
      "LN-dc",
      "HN-end",
      "kj-sa",
      "kj-HN",
      "kj-dc"
  );

  public static List<String> mainInput = Arrays.asList(
      "TR-start",
      "xx-JT",
      "xx-TR",
      "hc-dd",
      "ab-JT",
      "hc-end",
      "dd-JT",
      "ab-dd",
      "TR-ab",
      "vh-xx",
      "hc-JT",
      "TR-vh",
      "xx-start",
      "hc-ME",
      "vh-dd",
      "JT-bm",
      "end-ab",
      "dd-xx",
      "end-TR",
      "hc-TR",
      "start-vh"
  );


  public static void main(String[] args) {
    var testGraph = Day12Graph.createGraph(mainInput);
    long start = System.currentTimeMillis();
//    var journey = testGraph.getAllJourneys(1000000);
    var count = testGraph.depthFirstSearch();
    long end = System.currentTimeMillis();
    System.out.println(end - start);
    return;
  }

}

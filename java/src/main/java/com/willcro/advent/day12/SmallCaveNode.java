package com.willcro.advent.day12;

import java.util.ArrayList;
import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(of = "id")
@Data
public class SmallCaveNode implements Node {

  private final List<Node> nextSteps = new ArrayList<>();
  private final String id;

  public SmallCaveNode(String id) {
    this.id = id;
  }

  @Override
  public void addNextStep(Node node) {
    if (!nextSteps.contains(node)) {
      nextSteps.add(node);
    }
  }

  public String toString() {
    return id;
  }
}

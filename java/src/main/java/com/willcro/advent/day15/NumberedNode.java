package com.willcro.advent.day15;

import com.willcro.advent.day12.Node;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(of = {"x","y"})
@Data
public class NumberedNode implements Node {

  private final Integer x;
  private final Integer y;
  private final Integer value;
  private List<Node> nextSteps = new ArrayList<>();
  private boolean isEnd = false;

  public NumberedNode(Integer x, Integer y, Integer value) {
    this.x = x;
    this.y = y;
    this.value = value;
  }

  @Override
  public void addNextStep(Node node) {
    if (!nextSteps.contains(node)) {
      nextSteps.add(node);
    }
//    // prioritize right and down over left and up
//    nextSteps = nextSteps.stream()
//        .map(it -> (NumberedNode) it)
//        .sorted(Comparator.comparing(NumberedNode::getX).reversed().thenComparing(NumberedNode::getY).reversed())
//        .collect(Collectors.toList());
  }

  @Override
  public List<Node> getNextSteps() {
    return nextSteps;
  }

  @Override
  public String getId() {
    return "("+this.x+","+this.y+")";
  }

  public String toString() {
    return "("+this.x+","+this.y+")";
  }

}

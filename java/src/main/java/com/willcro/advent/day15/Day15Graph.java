package com.willcro.advent.day15;

import com.willcro.advent.day12.BigCaveNode;
import com.willcro.advent.day12.DeadEndException;
import com.willcro.advent.day12.EndNode;
import com.willcro.advent.day12.Journey;
import com.willcro.advent.day12.Node;
import com.willcro.advent.day12.SmallCaveNode;
import com.willcro.advent.day12.StartNode;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.regex.Pattern;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
public class Day15Graph {

  private Node start;
  private Node end;

  public int depthFirstSearch() {
    var lowestScore = 1800;
    var journey = new Day15Journey();
    journey.visitNode(start);

    try {
      while (true) {
        while (!journey.isFinished()) {
          var nextSteps = journey.getNextSteps();
          if (nextSteps.isEmpty()) {
            journey.backtrack();
          } else if (journey.getCurrentValue() > lowestScore) {
            journey.backtrack();
          } else {
            journey.visitNode(nextSteps.get(0));
          }
        }

        lowestScore = Math.min(lowestScore, journey.getCurrentValue());
        journey.backtrack();
      }
    } catch (DeadEndException ex) {
      return lowestScore;
    }
  }

  @EqualsAndHashCode
  private static class KeyValue<K, V extends Comparable> implements Comparable<KeyValue<K, V>> {

    private K key;
    private V value;

    KeyValue(K key, V value) {
      this.key = key;
      this.value = value;
    }

    @Override
    public int compareTo(KeyValue<K, V> o) {
      return this.value.compareTo(o.value);
    }
  }

  public static void main(String[] args) {
    SortedMap<Integer, String> uables = new TreeMap<>();
    SortedSet<KeyValue<String, Integer>> set = new TreeSet<>();
    set.add(new KeyValue<>("test", 1));
    set.add(new KeyValue<>("test", 2));
    return;
  }

  public int dijkstraSearch() {
    Set<Node> visitedNodes = new HashSet<>();

    Map<Node, Integer> dist = new HashMap<>();
    Map<Node, Integer> uables = new HashMap<>();

    Map<Node, Node> prev = new HashMap<>();
    dist.put(this.start, 0);
    uables.put(this.start, 0);

    var u = this.start;

    while (true) {
      u = uables.entrySet().stream().min(Comparator.comparing(Entry::getValue)).get().getKey();
      visitedNodes.add(u);
      uables.remove(u);

      if (((NumberedNode) u).isEnd()) {
        return dist.get(u);
      }

      for (Node node : u.getNextSteps()) {
        if (!visitedNodes.contains(node)) {
          var alt = dist.get(u) + ((NumberedNode) node).getValue();
          if (alt < dist.getOrDefault(node, Integer.MAX_VALUE)) {
            dist.put(node, alt);
            uables.put(node, alt);
            prev.put(node, u);
          }
        }
      }
    }
  }

}

package com.willcro.advent.day15;

import com.willcro.advent.day12.BigCaveNode;
import com.willcro.advent.day12.DeadEndException;
import com.willcro.advent.day12.EndNode;
import com.willcro.advent.day12.Node;
import com.willcro.advent.day12.SmallCaveNode;
import com.willcro.advent.day12.StartNode;
import java.util.Comparator;
import java.util.List;
import java.util.Stack;
import java.util.stream.Collectors;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class Day15Journey {

  private Stack<Node> nodes = new Stack<>();
  private boolean finished = false;

  public boolean nextNodeValid(Node node) {
    return !finished && !nodes.contains(node);
  }

  public void visitNode(Node node) {
    if (!nextNodeValid(node)) {
      throw new RuntimeException("Node is not valid for this journey");
    } else {
      nodes.push(node);
    }
    if (node instanceof EndNode || (node instanceof NumberedNode && ((NumberedNode) node).isEnd())) {
      finished = true;
    }
  }

  public boolean isFinished() {
    return finished;
  }

  public String toString() {
    return nodes.stream().map(Node::getId).collect(Collectors.joining(","));
  }

  public List<Node> getNextSteps() throws DeadEndException {
    if (nodes.isEmpty()) {
      throw new DeadEndException("Our back is against the wall");
    }
    var currentNode = nodes.peek();
    return currentNode.getNextSteps().stream()
        .filter(this::nextNodeValid)
        .map(it -> (NumberedNode) it)
        .sorted(Comparator.comparing(NumberedNode::getX).thenComparing(NumberedNode::getY).reversed())
        .collect(Collectors.toList());
  }

  public void backtrack() throws DeadEndException {
    if (nodes.isEmpty()) {
      throw new DeadEndException("Our back is against the wall");
    }

    finished = false;
    var popped = nodes.pop();
    var nextSteps = getNextSteps();
    var index = nextSteps.indexOf(popped);

    // if this was the last option
    if (index == nextSteps.size() - 1) {
      backtrack();
    } else {
      visitNode(nextSteps.get(index + 1));
    }
  }

  public Integer getCurrentValue() {
    return nodes.stream()
        .filter(it -> it instanceof NumberedNode)
        .map(it -> (NumberedNode) it)
        .mapToInt(NumberedNode::getValue)
        .sum() - ((NumberedNode) nodes.firstElement()).getValue();
  }
}

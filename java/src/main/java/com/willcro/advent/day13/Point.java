package com.willcro.advent.day13;

import java.util.regex.Pattern;
import lombok.Data;

@Data
public class Point {

  private final Integer x;
  private final Integer y;

  public Point flipX(Integer xLine) {
    var newX = xLine - Math.abs(this.x - xLine);
    return new Point(newX, this.y);
  }

  public Point flipY(Integer yLine) {
    var newY = yLine - Math.abs(this.y - yLine);
    return new Point(this.x, newY);
  }

  public static Point from(String str) {
    var pattern = Pattern.compile("([0-9]+),([0-9]+)");
    var matcher = pattern.matcher(str);
    if (matcher.matches()) {
      var x = Integer.parseInt(matcher.group(1));
      var y = Integer.parseInt(matcher.group(2));
      return new Point(x, y);
    }
    throw new IllegalArgumentException("Input not in expected format");
  }

}

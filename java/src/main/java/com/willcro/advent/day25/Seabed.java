package com.willcro.advent.day25;

import static com.willcro.advent.day25.CucumberType.BLANK;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
public class Seabed {

  private final List<List<CucumberType>> cucumbers;

  public Seabed(Integer width, Integer height) {
    this.cucumbers = getEmpty(width, height);
  }

  public Seabed(List<List<CucumberType>> cucumbers) {
    this.cucumbers = cucumbers;
  }

  private static final Map<String, CucumberType> iconToCucumber = Map.of(
      ">", CucumberType.EAST,
      "v", CucumberType.SOUTH,
      ".", BLANK
  );

  public static Seabed parse(String input) {
    return new Seabed(Arrays.stream(input.split("\n")).map(line -> Arrays.stream(line.split("")).map(iconToCucumber::get).collect(
        Collectors.toList())).collect(Collectors.toList()));
  }

  private List<List<CucumberType>> getEmpty(Integer width, Integer height) {
    var rows = new ArrayList<List<CucumberType>>();
    for (var y=0; y<height; y++) {
      var row = new ArrayList<CucumberType>();
      for (var x=0; x<width; x++) {
        row.add(BLANK);
      }
      rows.add(row);
    }
    return rows;
  }

  public Seabed advanceEast() {
    var height = cucumbers.size();
    var width = cucumbers.get(0).size();
    var newCukes = getEmpty(width, height);
    for (var y=0; y<cucumbers.size(); y++) {
      var row = cucumbers.get(y);
      for (var x=row.size()-1; x>=0; x--) {
        var cuke = row.get(x);
        if (CucumberType.EAST.equals(cuke)) {
          if (row.get((x + 1) % width) == BLANK) {
            newCukes.get(y).set((x + 1) % width, CucumberType.EAST);
          } else {
            newCukes.get(y).set(x, CucumberType.EAST);
          }
        } else if(CucumberType.SOUTH.equals(cuke)) {
          newCukes.get(y).set(x, cuke);
        }
      }
    }
    return new Seabed(newCukes);
  }

  public Seabed advanceSouth() {
    var height = cucumbers.size();
    var width = cucumbers.get(0).size();
    var newCukes = getEmpty(width, height);
    for (var y=cucumbers.size()-1; y>=0; y--) {
      var row = cucumbers.get(y);
      for (var x=0; x<row.size(); x++) {
        var cuke = row.get(x);
        if (CucumberType.SOUTH.equals(cuke)) {
          if (cucumbers.get((y + 1) % height).get(x) == BLANK) {
            newCukes.get((y + 1) % height).set(x, CucumberType.SOUTH);
          } else {
            newCukes.get(y).set(x, CucumberType.SOUTH);
          }
        } else if(CucumberType.EAST.equals(cuke)) {
          newCukes.get(y).set(x, cuke);
        }
      }
    }
    return new Seabed(newCukes);
  }

  public void print() {
    cucumbers.stream().map(row -> row.stream().map(CucumberType::getSymbol).collect(Collectors.joining(""))).forEach(System.out::println);
  }
}

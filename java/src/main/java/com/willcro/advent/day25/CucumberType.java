package com.willcro.advent.day25;

public enum CucumberType {

  EAST(">"),
  SOUTH("v"),
  BLANK(".");

  public String getSymbol() {
    return symbol;
  }

  private final String symbol;

  CucumberType(String symbol) {
    this.symbol = symbol;
  }
}

package com.willcro.advent.day25;

public class SeaCucumber {

  private final CucumberType type;

  public SeaCucumber(CucumberType type) {
    this.type = type;
  }
}

package com.willcro.advent.day25;

import com.willcro.advent.day22.Day22;
import java.io.IOException;

public class Day25 {

  public static void main(String[] args) throws IOException {
    var in = Day25.class.getResourceAsStream("/day25main.txt");
    var inputString = new String(in.readAllBytes());
    var newSeabed = Seabed.parse(inputString);

    Seabed seabed = null;
    var stepCount = 0;

    do {
      newSeabed.print();
      stepCount++;
      seabed = newSeabed;
      newSeabed = seabed.advanceEast();
      newSeabed = newSeabed.advanceSouth();
    } while (!seabed.equals(newSeabed));

    System.out.println("Steps: " + stepCount);

    return;
  }
}

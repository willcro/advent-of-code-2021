package com.willcro.advent.day14;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.stream.Collectors;

public class Day14Part2 {

  public static void main(String[] args) {
    var rules = Day14.parseInput(Day14.mainInput);
    var transforms = createInitialTransforms(rules);

    var stepCount = 40;
    for (var i=0; i<stepCount; i++ ) {
      transforms = runStep(transforms, rules);
    }

    var output = calculateAnswer("HBCHSNFFVOBNOFHFOBNO", transforms);
    System.out.println(output);
  }

  private static Map<String, Transform> runStep(Map<String, Transform> transforms, Map<String, String> rules) {
    var out = new HashMap<String, Transform>();
    transforms.forEach((key, transform) -> {
      if (rules.containsKey(key)) {
        var newChar = rules.get(key);

        var key1 = key.substring(0, 1) + newChar;
        var t1 = transforms.getOrDefault(key1, new Transform(key1));

        var key2 = newChar + key.substring(1);
        var t2 = transforms.getOrDefault(key2, new Transform(key2));

        out.put(key, t1.plus(t2));
      } else {
        out.put(key, transform);
      }
    });
    return out;
  }

  private static Map<String, Transform> createInitialTransforms(Map<String, String> rules) {
    return rules.keySet().stream().collect(Collectors.toMap(it -> it, Transform::new));
  }

  private static Long calculateAnswer(String start, Map<String, Transform> transforms) {
    var startPair = start.substring(0, 2);
    var finalTransform = transforms.getOrDefault(startPair, new Transform(startPair));
    for (var i=1; i<start.length() - 1; i++) {
      var pair = start.substring(i, i+2);
      var transform = transforms.getOrDefault(pair, new Transform(pair));
      finalTransform = finalTransform.plus(transform);
    }
    return finalTransform.getAnswer();
  }

}

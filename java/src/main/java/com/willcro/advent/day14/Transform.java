package com.willcro.advent.day14;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

public class Transform {

  public static void main(String[] args) {
    var tran1 = new Transform("AB");
    var tran2 = new Transform("BC");
    var sum = tran1.plus(tran2);
    return;
  }

  private Map<String, Long> counts;
  private Integer steps;
  private String input;

  public Transform(String input) {
    this.input = input;
    this.counts = Arrays.stream(input.split(""))
        .collect(Collectors.groupingBy(it -> it, Collectors.counting()));
    this.steps = 0;
  }

  private Transform(String input, Map<String, Long> counts, Integer steps) {
    this.input = input;
    this.counts = counts;
    this.steps = steps;
  }

  public Transform plus(Transform other) {
    // other is adjacent to this one
    if (this.input.charAt(1) != other.input.charAt(0)) {
      throw new IllegalArgumentException("The two transforms must be adjacent");
    }

    var newInput = this.input.substring(0, 1) + (other.input.substring(1));

    // add up the two counts
    var keys = new HashSet<>(this.counts.keySet());
    keys.addAll(other.counts.keySet());
    var newCounts = new HashMap<String, Long>();
    keys.forEach(key -> newCounts.put(key, this.counts.getOrDefault(key, 0L) + other.counts.getOrDefault(key, 0L)));

    // remove the duplicated character
    newCounts.put(this.input.substring(1), newCounts.get(this.input.substring(1)) - 1);

    return new Transform(newInput, newCounts, this.steps + 1);
  }

  public Long getAnswer() {
    var mostCommon = counts.entrySet().stream()
        .max(Comparator.comparing(Entry::getValue)).get().getValue();

    var leastCommon = counts.entrySet().stream()
        .min(Comparator.comparing(Entry::getValue)).get().getValue();

    return mostCommon - leastCommon;
  }

}

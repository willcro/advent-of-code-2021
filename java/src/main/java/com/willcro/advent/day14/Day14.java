package com.willcro.advent.day14;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Day14 {
  public static final String testInput = "CH -> B\n"
      +"HH -> N\n"
      +"CB -> H\n"
      +"NH -> C\n"
      +"HB -> C\n"
      +"HC -> B\n"
      +"HN -> C\n"
      +"NN -> C\n"
      +"BH -> H\n"
      +"NC -> B\n"
      +"NB -> B\n"
      +"BN -> B\n"
      +"BB -> N\n"
      +"BC -> B\n"
      +"CC -> N\n"
      +"CN -> C";

  public static final String mainInput = "HF -> O\n"
      + "KF -> F\n"
      + "NK -> F\n"
      + "BN -> O\n"
      + "OH -> H\n"
      + "VC -> F\n"
      + "PK -> B\n"
      + "SO -> B\n"
      + "PP -> H\n"
      + "KO -> F\n"
      + "VN -> S\n"
      + "OS -> B\n"
      + "NP -> C\n"
      + "OV -> C\n"
      + "CS -> P\n"
      + "BH -> P\n"
      + "SS -> P\n"
      + "BB -> H\n"
      + "PH -> V\n"
      + "HN -> F\n"
      + "KV -> H\n"
      + "HC -> B\n"
      + "BC -> P\n"
      + "CK -> P\n"
      + "PS -> O\n"
      + "SH -> N\n"
      + "FH -> N\n"
      + "NN -> P\n"
      + "HS -> O\n"
      + "CB -> F\n"
      + "HH -> F\n"
      + "SB -> P\n"
      + "NB -> F\n"
      + "BO -> V\n"
      + "PN -> H\n"
      + "VP -> B\n"
      + "SC -> C\n"
      + "HB -> H\n"
      + "FP -> O\n"
      + "FC -> H\n"
      + "KP -> B\n"
      + "FB -> B\n"
      + "VK -> F\n"
      + "CV -> P\n"
      + "VF -> V\n"
      + "SP -> K\n"
      + "CC -> K\n"
      + "HV -> P\n"
      + "NC -> N\n"
      + "VH -> K\n"
      + "PF -> P\n"
      + "PB -> S\n"
      + "BF -> K\n"
      + "FF -> C\n"
      + "FV -> V\n"
      + "KS -> H\n"
      + "VB -> F\n"
      + "SV -> F\n"
      + "HO -> B\n"
      + "FN -> C\n"
      + "SN -> F\n"
      + "OB -> N\n"
      + "KN -> P\n"
      + "BV -> H\n"
      + "ON -> N\n"
      + "NF -> S\n"
      + "OF -> P\n"
      + "NV -> S\n"
      + "VS -> C\n"
      + "OO -> C\n"
      + "BP -> H\n"
      + "BK -> N\n"
      + "CP -> N\n"
      + "PC -> K\n"
      + "CN -> H\n"
      + "KB -> B\n"
      + "BS -> P\n"
      + "KK -> P\n"
      + "SF -> V\n"
      + "CO -> V\n"
      + "CH -> P\n"
      + "FO -> B\n"
      + "FS -> F\n"
      + "VO -> H\n"
      + "NS -> F\n"
      + "KC -> H\n"
      + "VV -> K\n"
      + "NO -> P\n"
      + "OK -> F\n"
      + "PO -> V\n"
      + "FK -> H\n"
      + "OP -> H\n"
      + "PV -> N\n"
      + "CF -> P\n"
      + "NH -> K\n"
      + "SK -> O\n"
      + "KH -> P\n"
      + "HP -> V\n"
      + "OC -> V\n"
      + "HK -> F";

  public static void main(String[] args) {
//    var startInput = "NNCB";
    var startInput = "HBCHSNFFVOBNOFHFOBNO";
    var rules = parseInput(mainInput);

    var stepCount = 40;
    String out = startInput;

    for (var i=0; i<stepCount; i++ ) {
      out = runStep(out, rules);
    }

    System.out.println(calculateAnswer(out));
  }

  private static long calculateAnswer(String input) {
    var counts = Stream.of(input.split(""))
        .collect(Collectors.groupingBy(it -> it, Collectors.counting()));

    var mostCommon = counts.entrySet().stream()
        .max(Comparator.comparing(Entry::getValue)).get().getValue();

    var leastCommon = counts.entrySet().stream()
        .min(Comparator.comparing(Entry::getValue)).get().getValue();

    return mostCommon - leastCommon;

  }

  private static String runStep(String input, Map<String, String> rules) {
    var output = new StringBuilder(input);
    for (var i=input.length() - 2; i>=0; i--) {
      var pair = input.substring(i, i+2);
      var insertion = rules.get(pair);
      if (insertion != null) {
        output.insert(i + 1, insertion);
      }
    }
    return output.toString();
  }

  public static Map<String, String> parseInput(String input) {
    var pattern = Pattern.compile("(\\w{2}) -> (\\w{1})");
    return Stream.of(input.split("\n"))
        .map(pattern::matcher)
        .filter(Matcher::matches)
        .collect(Collectors.toMap(line -> line.group(1), line -> line.group(2)));
  }

}

package com.willcro.advent.day21;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class Day21 {

  private static Integer dieNum = 1;
  private static Integer rolls = 0;

  public static void main(String[] args) {
//    var out = playGame();
//    System.out.println(out);
    playGamePart2();
  }

  private static Integer playGame() {
    var p1Score = 0;
    var p1Pos = 4;
    var p2Score = 0;
    var p2Pos = 8;

    while (true) {
      // p1 turn
      var roll = roll() + roll() + roll();
      p1Pos = (p1Pos + roll) % 10;
      if (p1Pos == 0) {
        p1Pos = 10;
      }
      p1Score += p1Pos;
      if (p1Score >= 1000) {
        return rolls * p2Score;
      }

      // p2 turn
      roll = roll() + roll() + roll();
      p2Pos = (p2Pos + roll) % 10;
      if (p2Pos == 0) {
        p2Pos = 10;
      }
      p2Score += p2Pos;
      if (p2Score >= 1000) {
        return rolls * p1Score;
      }
    }
  }

  private static void playGamePart2() {
    var p1Score = 0;
    var p1Pos = 9;
    var p2Score = 0;
    var p2Pos = 10;

    long p1Wins = 0;
    long p2Wins = 0;

    var possibleRolls = new HashMap<Integer, Long>();
    possibleRolls.put(3, 1L);
    possibleRolls.put(4, 3L);
    possibleRolls.put(5, 6L);
    possibleRolls.put(6, 7L);
    possibleRolls.put(7, 6L);
    possibleRolls.put(8, 3L);
    possibleRolls.put(9, 1L);

    Map<GameState, Long> gameStates = new HashMap<GameState, Long>();
    gameStates.put(new GameState(p1Score, p1Pos, p2Score, p2Pos), 1L);

    while (!gameStates.isEmpty()) {
      // p1 rolls
      var newGameStates = gameStates.entrySet().stream().flatMap(entry -> {
        var state = entry.getKey();
        var count = entry.getValue();

        return possibleRolls.entrySet().stream().map(roll -> {
          var spacesToMove = roll.getKey();
          var multiplicity = roll.getValue();
          var newPosition = mod(state.getP1Position() + spacesToMove, 10);
          var newState = new GameState(state.getP1Score() + newPosition, newPosition, state.getP2Score(), state.getP2Position());
          return Map.entry(newState, multiplicity * count);
        });
      }).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, Long::sum));

      p1Wins += newGameStates.entrySet().stream()
          .filter(entry -> entry.getKey().getP1Score() >= 21)
          .mapToLong(Map.Entry::getValue).sum();

      gameStates = newGameStates.entrySet().stream()
          .filter(entry1 -> entry1.getKey().getP1Score() < 21)
          .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

      // p2 rolls
      newGameStates = gameStates.entrySet().stream().flatMap(entry -> {
        var state = entry.getKey();
        var count = entry.getValue();

        return possibleRolls.entrySet().stream().map(roll -> {
          var spacesToMove = roll.getKey();
          var multiplicity = roll.getValue();
          var newPosition = mod(state.getP2Position() + spacesToMove, 10);
          var newState = new GameState(state.getP1Score(), state.getP1Position(), state.getP2Score() + newPosition, newPosition);
          return Map.entry(newState, multiplicity * count);
        });
      }).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, Long::sum));

      p2Wins += newGameStates.entrySet().stream()
          .filter(entry -> entry.getKey().getP2Score() >= 21)
          .mapToLong(Map.Entry::getValue).sum();

      gameStates = newGameStates.entrySet().stream()
          .filter(entry -> entry.getKey().getP2Score() < 21)
          .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    System.out.println("P1 wins: " + p1Wins);
    System.out.println("P2 wins: " + p2Wins);
  }

  private static Integer roll() {
    rolls++;
    var out = dieNum;
    dieNum = (dieNum + 1) % 100;
    if (dieNum == 0) {
      dieNum = 100;
    }

    return out;
  }

  private static Integer mod(Integer num, Integer mod) {
    var out = num % mod;
    return out == 0 ? mod : out;
  }

}

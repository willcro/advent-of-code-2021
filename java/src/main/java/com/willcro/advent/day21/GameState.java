package com.willcro.advent.day21;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class GameState {

  private Integer p1Score;
  private Integer p1Position;
  private Integer p2Score;
  private Integer p2Position;

}

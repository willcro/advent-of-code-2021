package com.willcro.advent.day23;

import static com.willcro.advent.day23.Species.*;

public enum Space {
  H1(true, null, null),
  H2(true, null, null),
  H3(true, null, null),
  H4(true, null, null),
  H5(true, null, null),
  H6(true, null, null),
  H7(true, null, null),
  DA(false, null, null),
  DB(false, null, null),
  DC(false, null, null),
  DD(false, null, null),
  RA1(false, A, 1),
  RA2(false, A, 2),
  RA3(false, A, 3),
  RA4(false, A, 4),

  RB1(false, B, 1),
  RB2(false, B, 2),
  RB3(false, B, 3),
  RB4(false, B, 4),

  RC1(false, C, 1),
  RC2(false, C, 2),
  RC3(false, C, 3),
  RC4(false, C, 4),

  RD1(false, D, 1),
  RD2(false, D, 2),
  RD3(false, D, 3),
  RD4(false, D, 4);

  public final boolean isHallway;
  public final Species homeFor;

  public Integer getDistanceFromEntrance() {
    return distanceFromEntrance;
  }

  public final Integer distanceFromEntrance;

  Space(boolean isHallway, Species homeFor, Integer distanceFromEntrance) {
    this.isHallway = isHallway;
    this.homeFor = homeFor;
    this.distanceFromEntrance = distanceFromEntrance;
  }
}

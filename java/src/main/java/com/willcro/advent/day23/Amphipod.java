package com.willcro.advent.day23;

import static com.willcro.advent.day23.Space.*;
import static com.willcro.advent.day23.Species.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.Data;

@Data
public class Amphipod {

  private final Species species;
  private Space currentSpace;
  private boolean returnedHome = false;

  public Amphipod(Species species, Space currentSpace) {
    this.species = species;
    this.currentSpace = currentSpace;
  }

  public List<GameMove> getAllowedMoves(GameBoard board) {
    if (myRoomComplete(board) || returnedHome) {
      return Collections.emptyList();
    }

    return getAllowedSpaces(board).stream()
        .filter(space -> !space.equals(currentSpace))
        .filter(space -> !species.equals(space.homeFor) || !isHome() || this.currentSpace.distanceFromEntrance == null || space.distanceFromEntrance > this.currentSpace.distanceFromEntrance)
//        .filter(space -> space.distanceFromEntrance == null || this.currentSpace.distanceFromEntrance == null || space.distanceFromEntrance > this.currentSpace.distanceFromEntrance)
        .map(space -> SpaceUtils.getPath(currentSpace, space))
        .filter(spaces -> spaces.stream().noneMatch(board.getOccupiedSpaces()::contains))
        .map(spaces -> new GameMove(this, spaces.get(spaces.size() - 1), this.species.cost * spaces.size()))
        .collect(Collectors.toList());
  }

  private static Map<Species, List<Space>> speciesToHomeSpaces = Map.of(
      A, Arrays.asList(RA1, RA2, RA3, RA4),
      B, Arrays.asList(RB1, RB2, RB3, RB4),
      C, Arrays.asList(RC1, RC2, RC3, RC4),
      D, Arrays.asList(RD1, RD2, RD3, RD4)

//      A, Arrays.asList(RA1, RA2),
//      B, Arrays.asList(RB1, RB2),
//      C, Arrays.asList(RC1, RC2),
//      D, Arrays.asList(RD1, RD2)
  );

//  private static Set<Space> deepSpaces = Set.of(RA2, RB2, RC2, RD2);
  private static Set<Space> deepSpaces = Set.of(RA4, RB4, RC4, RD4);

  private List<Space> getAllowedSpaces(GameBoard board) {
    // if you are already deep in your own home, you should never need to leave
    if (isHome() && deepSpaces.contains(currentSpace)) {
      return Collections.emptyList();
    }

    var allowed = new ArrayList<Space>();
    if (!inHallway()) {
      allowed.addAll(Arrays.asList(H1, H2, H3, H4, H5, H6, H7));
    }

    if (!someonesInMyHouse(board)) {
      speciesToHomeSpaces.get(this.species).stream()
          .filter(space -> !board.getOccupiedSpaces().contains(space))
          .max(Comparator.comparing(Space::getDistanceFromEntrance)).ifPresent(allowed::add);
    }

    return allowed;
  }

  private boolean someonesInMyHouse(GameBoard board) {
    return board.getAmphipods().stream()
        .filter(amp -> amp.species != this.species)
        .anyMatch(amp -> this.species.equals(amp.getCurrentSpace().homeFor));
  }

  private boolean inHallway() {
    return currentSpace.isHallway;
  }

  public boolean isHome() {
    return species.equals(currentSpace.homeFor);
  }

  private boolean myRoomComplete(GameBoard board) {
    return board.getAmphipods().stream()
        .filter(player -> player.getSpecies().equals(this.species))
        .allMatch(Amphipod::isHome);
  }

  private static Map<Species, Space> homes = Map.of(
      A, RA1,
      B, RB1,
      C, RC1,
      D, RD1
  );

  private boolean blockingSomeone(GameBoard gameBoard) {
    if (!isHome()) {
      return false;
    }
    return gameBoard.getAmphipods().stream()
        .filter(a -> !a.isHome())
        .filter(a -> this.species.equals(a.currentSpace.homeFor))
        .anyMatch(a -> a.currentSpace.distanceFromEntrance > this.currentSpace.distanceFromEntrance);
  }

  public Integer h(GameBoard gameBoard) {
    if (this.isHome()) {
      if (blockingSomeone(gameBoard)) {
        return (this.currentSpace.distanceFromEntrance + 3) * species.cost;
      } else {
        return 0;
      }
    }
    return SpaceUtils.getPath(this.currentSpace, homes.get(species)).size() * species.cost;
  }
}

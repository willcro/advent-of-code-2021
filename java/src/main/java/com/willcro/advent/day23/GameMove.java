package com.willcro.advent.day23;

import lombok.Data;

@Data
public class GameMove {

  private final Amphipod player;
  private final Space newSpace;
  private final Integer cost;

}

package com.willcro.advent.day23;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.stream.Collectors;

public class Thing {

  public Integer test(Set<Integer> visitedBoards, Map<Integer, Integer> dist, SortedSet<GameBoard> uables) {

//    Map<GameBoard, GameBoard> prev = new HashMap<>();
    var min = 0;
    while (true) {
      var u = uables.first();
      if (u.getHeuristic() > min) {
        min = u.getHeuristic();
        System.out.println("New min: " + min);
      }
      uables.remove(u);
      visitedBoards.add(u.hashCode());
      if (u.gameComplete()) {
        return dist.get(u.hashCode());
      }

      var moves = u.getMoves();

      for (GameMove move : moves) {
        var newBoard = u.makeMove(move);
        if (!visitedBoards.contains(newBoard.hashCode())) {
          if (newBoard.getCost() < dist.getOrDefault(newBoard.hashCode(), Day23.MAX_SCORE)) {
            dist.put(newBoard.hashCode(), newBoard.getCost());
//            prev.put(newBoard, u);
            uables.add(newBoard);
          }
        }
      }
    }
  }

}

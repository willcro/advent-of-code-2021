package com.willcro.advent.day23;

import static com.willcro.advent.day23.Space.*;
import static com.willcro.advent.day23.Species.*;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Queue;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

public class Day23 {

  public static final Integer MAX_SCORE = 45000;
  private static final Integer MAX_DEPTH = 10;

  public static void main(String[] args) {
    var board = createTestGameBoard();
//    var cost = search(board, 0, 1);
    var cost = aStarSearch(board);
    System.out.println("Cost: " + cost);
  }

  private static GameBoard createTestGameBoard() {
    var players = Arrays.asList(
        new Amphipod(A, RA4),
        new Amphipod(A, RD4),
        new Amphipod(B, RA1),
        new Amphipod(B, RC1),
        new Amphipod(C, RB1),
        new Amphipod(C, RC4),
        new Amphipod(D, RB4),
        new Amphipod(D, RD1),

        // PART 2
        new Amphipod(A, RC3),
        new Amphipod(A, RD2),
        new Amphipod(B, RB3),
        new Amphipod(B, RC2),
        new Amphipod(C, RB2),
        new Amphipod(C, RD3),
        new Amphipod(D, RA2),
        new Amphipod(D, RA3)
    );
    return new GameBoard(players);
  }

  private static GameBoard createMainGameBoard() {
    var players = Arrays.asList(
        new Amphipod(A, RC4),
        new Amphipod(A, RD4),
        new Amphipod(B, RA4),
        new Amphipod(B, RB1),
        new Amphipod(C, RA1),
        new Amphipod(C, RB4),
        new Amphipod(D, RC1),
        new Amphipod(D, RD1),

        // PART 2
        new Amphipod(A, RC3),
        new Amphipod(A, RD2),
        new Amphipod(B, RB3),
        new Amphipod(B, RC2),
        new Amphipod(C, RB2),
        new Amphipod(C, RD3),
        new Amphipod(D, RA2),
        new Amphipod(D, RA3)
    );
    return new GameBoard(players);
  }

  private static Integer search(GameBoard board, Integer cost, Integer depth) {
    if (depth < 4) {
      System.out.println("Got depth " + depth);
    }

    if (cost >= MAX_SCORE) {
      return null;
    }

    if (board.gameComplete()) {
      return cost;
    }

    if (depth > MAX_DEPTH) {
      return null;
    }

    var moves = board.getMoves();

    return moves.stream().map(move -> {
      var newBoard = board.makeMove(move);
      return search(newBoard, cost + move.getCost(), depth + 1);
    }).filter(Objects::nonNull)
        .min(Integer::compareTo).orElse(null);
  }

  public static int aStarSearch(GameBoard board) {
    Set<Integer> visitedBoards = new HashSet<>();
    Map<Integer, Integer> dist = new HashMap<>();
    SortedSet<GameBoard> uables = new TreeSet<>(Comparator.comparing(GameBoard::getHeuristic).thenComparing(GameBoard::hashCode));

    dist.put(board.hashCode(), 0);
    uables.add(board);

    var thing = new Thing();
    while (true) {
      var ret = thing.test(visitedBoards, dist, uables);
      if (ret != null) {
        return ret;
      }
    }
  }

}

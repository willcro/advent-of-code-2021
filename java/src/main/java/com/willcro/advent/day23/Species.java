package com.willcro.advent.day23;

public enum Species {
  A(1),
  B(10),
  C(100),
  D(1000);

  public final Integer cost;

  private Species(Integer cost) {
    this.cost = cost;
  }
}

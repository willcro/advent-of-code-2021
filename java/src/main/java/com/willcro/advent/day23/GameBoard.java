package com.willcro.advent.day23;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
public class GameBoard {

  private List<Amphipod> amphipods;
  private Set<Space> occupiedSpaces;

  @EqualsAndHashCode.Exclude
  private Integer cost = 0;
  private Integer heuristic = 0;

  public GameBoard(List<Amphipod> amphipods) {
    this.amphipods = amphipods;
    this.occupiedSpaces = amphipods.stream()
        .map(Amphipod::getCurrentSpace)
        .collect(Collectors.toSet());
    this.heuristic = h();
  }

  public GameBoard(List<Amphipod> amphipods, Set<Space> occupiedSpaces, Integer cost) {
    this.amphipods = amphipods;
    this.occupiedSpaces = occupiedSpaces;
    this.cost = cost;
    this.heuristic = h();
  }

  public List<GameMove> getMoves() {
    var moves = amphipods.stream()
        .flatMap(amphipod -> amphipod.getAllowedMoves(this).stream())
        .collect(Collectors.toList());

    // if there are any available moves into home, do it now
    return moves.stream().filter(this::isMoveToHome).max(Comparator.comparing(GameMove::getCost))
        .map(Collections::singletonList)
        .orElse(moves);
  }

  private boolean isMoveToHome(GameMove move) {
    return move.getNewSpace().homeFor != null && move.getNewSpace().homeFor.equals(move.getPlayer().getSpecies());
  }

  // no side effects
  public GameBoard makeMove(GameMove move) {
    var players = new ArrayList<Amphipod>();
    players.addAll(amphipods);
    players.remove(move.getPlayer());

    var newPlayer = new Amphipod(move.getPlayer().getSpecies(), move.getNewSpace());
    newPlayer.setReturnedHome(!move.getPlayer().isHome() && newPlayer.isHome());

    players.add(new Amphipod(move.getPlayer().getSpecies(), move.getNewSpace()));

    var occupiedSpaces = new HashSet<>(this.occupiedSpaces);
    occupiedSpaces.remove(move.getPlayer().getCurrentSpace());
    occupiedSpaces.add(move.getNewSpace());

    var ret = new GameBoard(players, occupiedSpaces, this.cost + move.getCost());

    return ret;
  }

  public boolean gameComplete() {
    return amphipods.stream()
//        .filter(amp -> amp.getSpecies().equals(Species.D))
        .allMatch(Amphipod::isHome);
  }

  public Integer h() {
    var occupiedHomeSpaces = amphipods.stream()
        .filter(Amphipod::isHome)
        .map(Amphipod::getCurrentSpace)
        .collect(Collectors.toSet());
    var raw = amphipods.stream().mapToInt(a -> a.h(this)).sum();
    var adjustment = Arrays.stream(Space.values())
        .filter(space -> space.homeFor != null)
        .filter(space -> !occupiedHomeSpaces.contains(space))
        .mapToInt(space -> (space.distanceFromEntrance - 1) * space.homeFor.cost)
        .sum();
    return this.cost + raw + adjustment;
  }

  public Long myHashCode() {
    return 0L;
  }

}

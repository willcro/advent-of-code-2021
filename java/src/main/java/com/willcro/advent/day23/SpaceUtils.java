package com.willcro.advent.day23;

import static com.willcro.advent.day23.Space.*;
import static com.willcro.advent.day23.Space.values;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class SpaceUtils {

  private static final Map<Space, List<Space>> nextHops = new HashMap<>();
  private static Map<Space, Map<Space, List<Space>>> paths;

  static {
    nextHops.put(H1,  Arrays.asList(H2));
    nextHops.put(H2,  Arrays.asList(H1, DA));
    nextHops.put(H3,  Arrays.asList(DA, DB));
    nextHops.put(H4,  Arrays.asList(DB, DC));
    nextHops.put(H5,  Arrays.asList(DC, DD));
    nextHops.put(H6,  Arrays.asList(DD, H7));
    nextHops.put(H7,  Arrays.asList(H6));
    nextHops.put(DA,  Arrays.asList(H2, H3, RA1));
    nextHops.put(DB,  Arrays.asList(H3, H4, RB1));
    nextHops.put(DC,  Arrays.asList(H4, H5, RC1));
    nextHops.put(DD,  Arrays.asList(H5, H6, RD1));
    nextHops.put(RA1, Arrays.asList(DA, RA2));
    nextHops.put(RA2, Arrays.asList(RA1, RA3));
    nextHops.put(RB1, Arrays.asList(DB, RB2));
    nextHops.put(RB2, Arrays.asList(RB1, RB3));
    nextHops.put(RC1, Arrays.asList(DC, RC2));
    nextHops.put(RC2, Arrays.asList(RC1, RC3));
    nextHops.put(RD1, Arrays.asList(DD, RD2));
    nextHops.put(RD2, Arrays.asList(RD1, RD3));

    // PART 2
    nextHops.put(RA3, Arrays.asList(RA2, RA4));
    nextHops.put(RB3, Arrays.asList(RB2, RB4));
    nextHops.put(RC3, Arrays.asList(RC2, RC4));
    nextHops.put(RD3, Arrays.asList(RD2, RD4));

    nextHops.put(RA4, Arrays.asList(RA3));
    nextHops.put(RB4, Arrays.asList(RB3));
    nextHops.put(RC4, Arrays.asList(RC3));
    nextHops.put(RD4, Arrays.asList(RD3));

    calculatePaths();
  }

  private static void calculatePaths() {
    paths = Arrays.stream(values()).collect(Collectors.toMap(space -> space,
        space -> Arrays.stream(values()).collect(Collectors.toMap(it -> it, b -> calculatePath(space, b, space, Collections.emptyList())))));
  }

  private static List<Space> calculatePath(Space a, Space b, Space start, List<Space> visitedNodes) {
    if (a.equals(b)) {
      return visitedNodes;
    }
    return nextHops.get(a).stream()
        .filter(space -> !visitedNodes.contains(space))
        .filter(space -> !space.equals(start))
        .map(space -> {
          var visited = new ArrayList<Space>();
          visited.addAll(visitedNodes);
          visited.add(space);
          return calculatePath(space, b, start, visited);
        })
        .filter(Objects::nonNull)
        .findFirst().orElse(null);
  }

  public static List<Space> getPath(Space a, Space b) {
    return paths.get(a).get(b);
  }


}

// part 1

var rawInput = document.body.children[0].innerText.split("\n")

var test = `0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2`.split("\n");

function countOverlaps(input) {
    var grid = zeros2d(1000, 1000);
    input.map(parseLine).filter(l => l.x1 == l.x2 || l.y1 == l.y2).forEach(l => {
        var x1 = Math.min(l.x1, l.x2);
        var x2 = Math.max(l.x1, l.x2);
        var y1 = Math.min(l.y1, l.y2);
        var y2 = Math.max(l.y1, l.y2);
        for (x=x1; x<=x2; x++) {
            for (y=y1; y<=y2; y++) {
                grid[x][y]++;
            }
        }
    });
    // return grid;
    return grid.flatMap(it => it).filter(it => it > 1).length;
}

function parseLine(line) {
    var match = line.match(/(\d*),(\d*) -> (\d*),(\d*)/);
    return {
        x1: match[1] * 1,
        y1: match[2] * 1,
        x2: match[3] * 1,
        y2: match[4] * 1
    }
}

function zeros2d(width, height) {
    var out = [];
    for (var i=0; i<width; i++) {
        out[i] = zeros(height);
    }
    return out;
}

function zeros(length) {
    var out = [];
    for (var i=0; i<length; i++) {
        out[i] = 0;
    }
    return out;
}

// part 2

function countOverlaps2(input) {
    var grid = zeros2d(1000, 1000);
    var lines = input.map(parseLine);
    
    lines.filter(l => l.x1 == l.x2 || l.y1 == l.y2).forEach(l => {
        var x1 = Math.min(l.x1, l.x2);
        var x2 = Math.max(l.x1, l.x2);
        var y1 = Math.min(l.y1, l.y2);
        var y2 = Math.max(l.y1, l.y2);
        for (x=x1; x<=x2; x++) {
            for (y=y1; y<=y2; y++) {
                grid[x][y]++;
            }
        }
    });

    lines.filter(l => l.x1 != l.x2 && l.y1 != l.y2).forEach(l => {
        var xdir = (l.x2 - l.x1) / Math.abs(l.x2 - l.x1);
        var ydir = (l.y2 - l.y1) / Math.abs(l.y2 - l.y1);
        var x = l.x1;
        var y = l.y1;
        grid[x][y]++;

        do {
            x += xdir;
            y += ydir;
            grid[x][y]++;
        } while (x != l.x2);
    });

    // return grid;
    return grid.flatMap(it => it).filter(it => it > 1).length;
}

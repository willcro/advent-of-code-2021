// part 1
var rawInput = document.body.children[0].innerText.split("\n");
rawInput.pop();

var test = `2199943210
3987894921
9856789892
8767896789
9899965678`.split("\n");

function calculateRisk(input) {
    var data = input.map(row => row.split("").map(it => it * 1));
    var width = data[0].length;
    var height = data.length;
    var xmax = width - 1;
    var ymax = height - 1;
    var ret = 0;
    for (var y=0; y<height; y++) {
        for (var x=0; x<width; x++) {
            var isLowPoint = true;
            var it = data[y][x];

            // check up
            if (y != 0 && it >= data[y-1][x]) {
                isLowPoint = false;
            }

            // check down
            if (y != ymax && it >= data[y+1][x]) {
                isLowPoint = false;
            }

            // check left
            if (x != 0 && it >= data[y][x-1]) {
                isLowPoint = false;
            }

            // check right
            if (x != xmax && it >= data[y][x+1]) {
                isLowPoint = false;
            }

            if (isLowPoint) {
                ret += (it + 1)
            }
        }
    }
    return ret;
}

// part 2

// returns coords of the local min
function findWatershed(data, x, y) {
    var it = data[y][x];
    var width = data[0].length;
    var height = data.length;
    var xmax = width - 1;
    var ymax = height - 1;
    if (it == 9) {
        return null;
    }
    // check up
    if (y != 0 && it > data[y-1][x]) {
        return findWatershed(data, x, y-1);
    }
    // check down
    if (y != ymax && it > data[y+1][x]) {
        return findWatershed(data, x, y+1);
    }
    // check left
    if (x != 0 && it > data[y][x-1]) {
        return findWatershed(data, x-1, y);
    }
    // check right
    if (x != xmax && it > data[y][x+1]) {
        return findWatershed(data, x+1, y);
    }

    // this is the watershed
    return {x: x, y: y};
}

function findLargestBasins(input) {
    var data = input.map(row => row.split("").map(it => it * 1));
    var width = data[0].length;
    var height = data.length;
    var ret = zeros2d(height, width);
    for (var y=0; y<height; y++) {
        for (var x=0; x<width; x++) {
            var watershed = findWatershed(data, x, y);
            if (watershed != null) {
                ret[watershed.y][watershed.x]++;
            }
        }
    }
    var arr = ret.flat().sort((a,b) => b-a);
    return arr[0] * arr[1] * arr[2];
}

function zeros2d(width, height) {
    var out = [];
    for (var i=0; i<width; i++) {
        out[i] = zeros(height);
    }
    return out;
}

function zeros(length) {
    var out = [];
    for (var i=0; i<length; i++) {
        out[i] = 0;
    }
    return out;
}













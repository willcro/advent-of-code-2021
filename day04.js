// part 1

var rawInput = document.body.children[0].innerText.split("\n")

function playBingo(input) {
    var boards = extractBoards(input);
    var picks = input[0].split(/\W+/).map(it => it * 1);
    var pickList = falses(100);

    for (var i=0; i<picks.length; i++) {
        pickList[picks[i]] = true;
        // console.log(pickList);
        var winners = boards.filter(board => checkBoard(board, pickList));
        if (winners.length > 0) {
            console.log("Winners in round " + i);
            console.log(winners);
            return winners[0].columns.flatMap(it => it).filter(num => !pickList[num]).reduce((a, b) => a+b) * picks[i];
            // return winners;
        }
    }

    return 0;
}

function checkBoard(board, pickList) {
    return board.rows.some(row => row.every(num => pickList[num])) || board.columns.some(col => col.every(num => pickList[num]));
}

function extractBoards(input) {
    var boards = []
    var boardNum = 0;
    var boardRow = 0;
    var currentBoard = [];
    for (var i=2; i<input.length; i++) {
        var line = input[i];
        if (line == "") {
            // starting a new board
            boards[boardNum] = currentBoard;
            currentBoard = [];
            boardNum++;
            boardRow = 0;
        } else {
            currentBoard[boardRow] = line.trim().split(/\W+/).map(it => it * 1);
            boardRow++;
        }
    }
    return boards.map(rawBoardToBetterBoard);
}

function rawBoardToBetterBoard(rawBoard) {
    var board = {};
    board.rows = rawBoard;
    var columns = [];
    for (var i=0; i<rawBoard[0].length; i++) {
        columns[i] = rawBoard.map(row => row[i]);
    }
    board.columns = columns;
    board.alreadyWon = false;
    return board;
}

function falses(length) {
    var out = [];
    for (var i=0; i<length; i++) {
        out[i] = 0;
    }
    return out;
}

// part 2

function playBingoToLose(input) {
    var boards = extractBoards(input);
    var picks = input[0].split(/\W+/).map(it => it * 1);
    var pickList = falses(100);
    var lastWinner;

    for (var i=0; i<picks.length; i++) {
        pickList[picks[i]] = true;
        var winners = boards.filter(board => checkBoard(board, pickList) && !board.alreadyWon);
        if (winners.length > 0) {
            console.log(winners);
            lastWinner = winners[0].columns.flatMap(it => it).filter(num => !pickList[num]).reduce((a, b) => a+b) * picks[i];
            winners.forEach(it => it.alreadyWon = true);
        }
    }

    return lastWinner;
}




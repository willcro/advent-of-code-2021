var rawInput = document.body.children[0].innerText.split("\n");
rawInput.pop();

var test = `be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce`.split("\n")

function parseInput(input) {
    return input.flatMap(parseLine)
}

function parseLine(line) {
    console.log(line);
    var match = line.match(/(.*) \| (.*)/);
    var signal = match[1].split(" ").map(digit => digit.split(""));
    var data = match[2].split(" ").map(digit => digit.split(""));
    var digits = determineDigits(signal);
    return parseData(data, digits);
}

function determineDigits(input) {
    var testDigits = input;
    var output = {};
    // uniques
    output[1] = testDigits.filter(digit => digit.length == 2)[0];
    output[4] = testDigits.filter(digit => digit.length == 4)[0];
    output[7] = testDigits.filter(digit => digit.length == 3)[0];
    output[8] = testDigits.filter(digit => digit.length == 7)[0];

    // 6 digits
    output[9] = testDigits.filter(digit => digit.length == 6 && intersectionSize(digit, output[4]) == 4)[0];
    output[0] = testDigits.filter(digit => digit.length == 6 && intersectionSize(digit, output[1]) == 2 && digit != output[9])[0];
    output[6] = testDigits.filter(digit => digit.length == 6 && digit != output[0] && digit != output[9])[0];

    // 5 digits
    output[3] = testDigits.filter(digit => digit.length == 5 && intersectionSize(digit, output[1]) == 2)[0];
    output[5] = testDigits.filter(digit => digit.length == 5 && intersectionSize(digit, output[4]) == 3 && digit != output[3])[0];
    output[2] = testDigits.filter(digit => digit.length == 5 && digit != output[3] && digit != output[5])[0];
    
    return output;
}

function parseData(data, digits) {
    return data.map(dataDig => {
        // console.log(dataDig);
        return Object.entries(digits).filter(dig => {
            return dataDig.length == dig[1].length && intersectionSize(dig[1], dataDig) == dataDig.length;
        })[0][0];
    });
}

function intersectionSize(array1, array2) {
    return array1.filter(elem => array2.includes(elem)).length;
}

// PART 2

function parseInput2(input) {
    return input.map(parseLine).map(arr => (arr[0] + arr[1] + arr[2] + arr[3]) * 1).reduce((a,b) => a + b);
}

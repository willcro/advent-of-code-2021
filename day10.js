var rawInput = document.body.children[0].innerText.split("\n");
rawInput.pop();

var test = `[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]`.split("\n");

function part1(input) {
    return input.map(parseLine).reduce((a,b) => a + b);
}

function parseLine(line) {
    var input = line.split("");
    var openers = ["(", "{", "[", "<"];
    var closers = [")", "}", "]", ">"];
    var scores = {
        ")": 3,
        "]": 57,
        "}": 1197,
        ">": 25137
    }

    var stack = [];
    for (var i=0; i<input.length; i++) {
        // console.log(stack);
        var char = input[i];
        if (openers.includes(char)) {
            stack.push(openers.indexOf(char));
        } else {
            var expectedCloser = stack.pop();
            if (closers[expectedCloser] != char) {
                return scores[char];
            }
        }
    }

    return 0;
}



// part 2

function isNotCorrupt(line) {
    return parseLine(line) == 0;
}

function scoreUnfinishedLine(line) {
    var input = line.split("");
    var openers = ["(", "{", "[", "<"];
    var closers = [")", "}", "]", ">"];
    var scores = {
        ")": 1,
        "]": 2,
        "}": 3,
        ">": 4
    }

    var stack = [];
    for (var i=0; i<input.length; i++) {
        // console.log(stack);
        var char = input[i];
        if (openers.includes(char)) {
            stack.push(openers.indexOf(char));
        } else {
            var expectedCloser = stack.pop();
            if (closers[expectedCloser] != char) {
                throw "Corrupted line"
            }
        }
    }

    return stack.reduceRight((acc, it) => (acc * 5) + scores[closers[it]], 0);
}

function part2(input) {
    return median(input.filter(isNotCorrupt).map(scoreUnfinishedLine));
}

function median(arr) {
    var sorted = arr.sort((a,b) => a-b);
    return sorted[Math.floor(arr.length / 2)];
}

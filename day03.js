// part 3

// get input list 
var nums = document.body.children[0].innerText.split("\n")

var test = [
"00100",
"11110",
"10110",
"10111",
"10101",
"01111",
"00111",
"11100",
"10000",
"11001",
"00010",
"01010"
];

function getPowerUsage(arr) {
    var counts = zeros(arr[0].length);
    arr.forEach(it => {
        for (var i=0; i<it.length; i++) {
            if (it[i] == "1") {
                counts[i]++;
            }
        }
    });
    var gamma = "0b";
    var epsilon = "0b";
    for (var i=0; i<arr[0].length; i++) {
        if (counts[i] > (arr.length) / 2) {
            gamma = gamma + "1";
            epsilon = epsilon + "0";
        } else {
            gamma = gamma + "0";
            epsilon = epsilon + "1";
        }
    }
    console.log(`gamma: ${gamma}, epsilon: ${epsilon}`);
    return gamma * epsilon;
}

function zeros(length) {
    var out = [];
    for (var i=0; i<length; i++) {
        out[i] = 0;
    }
    return out;
}

// part 2

function getOxygenRating(input) {
    var arr = input;
    
    // for each bit position
    for (var i=0; i<input[0].length; i++) {
        var count = 0;

        // for each input number
        for (var n=0; n<arr.length; n++) {
            if (arr[n][i] == "1") {
                count++;
            }
        }

        if (count >= (arr.length / 2)) {
            arr = arr.filter(it => it[i] == "1");
        } else {
            arr = arr.filter(it => it[i] == "0");
        }

    }
    
    return "0b" + arr[0];
}

function getCo2Rating(input) {
    var arr = input;
    
    // for each bit position
    for (var i=0; i<input[0].length; i++) {
        var count = 0;

        // for each input number
        for (var n=0; n<arr.length; n++) {
            if (arr[n][i] == "1") {
                count++;
            }
        }

        if (count >= (arr.length / 2)) {
            arr = arr.filter(it => it[i] == "0");
        } else {
            arr = arr.filter(it => it[i] == "1");
        }

        if (arr.length == 1) {
            return "0b" + arr[0];
        }
    }
    
    return "0b" + arr[0];
}

function getLifeSupportRating(input) {
    return getOxygenRating(input) * getCo2Rating(input);
}








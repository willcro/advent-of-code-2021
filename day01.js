/// PART 1

// get input list 
var nums = document.body.children[0].innerText.split("\n")

function countIncreasing(arr) {
    var increased = 0;
    for (var i=1; i<arr.length; i++) {
        if (arr[i] > arr[i-1]) {
            increased++;
        }
    }
    return increased;
}

countIncreasing(nums); // 1709


/// PART 2

function countIncreasingRollingSum(arr, windowSize) {
    var increased = 0;
    for (var i=1; i<=arr.length - windowSize; i++) {
        if (windowSum(arr, i, windowSize) > windowSum(arr, i-1, windowSize)) {
            increased++;
        }
    }
    return increased;
}

function windowSum(arr, startIndex, windowSize) {
    return arr.slice(startIndex, startIndex + windowSize).reduce((a, b) => a + b);
}

countIncreasingRollingSum(nums, 3) // 1761

// part 1

var rawInput = document.body.children[0].innerText.split("\n")[0].split(",").map(it => it * 1);

var test = [16,1,2,0,4,2,7,1,2,14];;

function findMiddle(input) {
    var sorted = input.sort();
    var min = sorted[0];
    var max = sorted[sorted.length - 1];
    var lowestCost = Infinity;
    var lowestCostPosition = Infinity;
    for (i=min; i<=max; i++) {
        var cost = sorted.map(it => Math.abs(it - i)).reduce((a,b) => a+b);
        if (cost < lowestCost) {
            lowestCost = cost;
            lowestCostPosition = i;
        }
    }
    return lowestCost;
}

// part 2

function distanceToCost(distance) {
    return ((distance * distance) / 2) + (distance / 2)
}

function findMiddle2(input) {
    var sorted = input.sort();
    var min = sorted[0];
    var max = sorted[sorted.length - 1];
    var lowestCost = Infinity;
    var lowestCostPosition = Infinity;
    for (i=min; i<=max; i++) {
        var cost = sorted.map(it => distanceToCost(Math.abs(it - i))).reduce((a,b) => a+b);
        if (cost < lowestCost) {
            lowestCost = cost;
            lowestCostPosition = i;
        }
    }
    return lowestCost;
}

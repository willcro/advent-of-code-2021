function runStep(x, y, dx, dy) {
  return {
    x: x + dx,
    y: y + dy,
    dx: Math.max(dx - 1, 0),
    dy: dy - 1
  }
}

function testVelocities(dx, dy, xmin, xmax, ymin, ymax) {
  var x=0;
  var y=0;
  while (x<=xmax && y>=ymin) {
    if (x >= xmin && x <= xmax && y >= ymin && y <= ymax) {
      return true;
    }
    var step = runStep(x, y, dx, dy);
    // console.log(step);
    x = step.x;
    y = step.y;
    dx = step.dx;
    dy = step.dy
  }
  return false;
}

function countVelocities() {
  var count = 0;
  for (var dx=0; dx<229; dx++) {
    for (var dy=-200; dy<200; dy++) {
      if (testVelocities(dx, dy, 175, 227, -134, -79)) {
        count++;
      }
    }
  }
  return count;
}
